package serwerkulaeater;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**Klasa reprezentująca okno aplikacji serwera.
 * @author K-E team
 *
 */

public class SerwerKulaEater extends JFrame{

	
	private static final long serialVersionUID = 1L;
	
	
	/**Pole wyboru portu.
	 * 
	 */
	private JTextField port;
	/**Pole prezentacji ilosci użytkowników.
	 * 
	 */
	private JTextField ilosc;
	/**Przycisk odpowiadający za stworzenie serwera. 
	 * 
	 */
	private JButton start;
	/**Przycisk odpowiadający za zakończenie serwera.
	 * 
	 */
	private JButton koniec;
	/**Pole reprezentujące aktualny obiekt klasy Serwer. 
	 * 
	 */
	private Serwer serwer;
	/**Pole prezentujące przesyłane komunikaty między serwerem a klientami.
	 * 
	 */
	private TextArea konsola;
	
	
	/**Konstruktor okna aplikacji serwera.
	 * 
	 */
	SerwerKulaEater(){
		super("SerwerKulaEater");
		setMinimumSize(new Dimension(600,300));
		init();
		pack();
		setVisible(true);
	}
	
	/**Metoda inicjująca okno aplikacji serwera, wołana z konstruktora klasy SerwerKulaEater.
	 * 
	 */
	private void init(){
		JPanel gornyPasek=new JPanel();
		gornyPasek.setLayout(new GridLayout(2,2));
		JLabel polecenie= new JLabel("Podaj port:");
		port= new JTextField(20);
		start= new JButton("START");
		start.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
					int numer=Integer.parseInt(port.getText());
					serwer= new Serwer(numer, konsola, ilosc);
					koniec.setEnabled(true);
					start.setEnabled(false);
				}catch (Exception p){		
					konsola.append("Błąd. Podaj inny port.\n");
				}				
			}
		});
		koniec=new JButton("ZAKOŃCZ");
		koniec.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				serwer.zakoncz();
				serwer.uaktualnijDane();
				serwer = null;
				koniec.setEnabled(false);
				start.setEnabled(true);
				konsola.append("Serwer zakończony.\n");
			}
		});
		koniec.setEnabled(false);
		konsola=new TextArea("",20,20,1);
		konsola.setEditable(false);
		ilosc=new JTextField("Zalogowanych użytkowników: 0");
		ilosc.setEditable(false);
        gornyPasek.add(polecenie);
		gornyPasek.add(port);
		gornyPasek.add(start);
		gornyPasek.add(koniec);
		setLayout(new BorderLayout());
		add(gornyPasek, BorderLayout.NORTH);
		add(konsola, BorderLayout.CENTER);
		add(ilosc,  BorderLayout.SOUTH);
		addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
            if(serwer!=null)
            	serwer.zakoncz();
            System.exit(0);
            }
         }); 
	}
	
	
	/**Główna metoda klasy.
	 * @param args
	 */
	public static void main(String args[]) {
		 javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
					try {
						@SuppressWarnings("unused")
						SerwerKulaEater g=new SerwerKulaEater();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		            
	            }
	     });
	}
}
