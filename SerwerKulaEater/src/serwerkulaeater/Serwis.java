package serwerkulaeater;

import java.io.*;
import java.net.*;
import java.util.StringTokenizer;

import serwerkulaeater.stale.ProtokolSieciowy;



public class Serwis implements Runnable {
	
	private int id;
	private Serwer serwer;
	private Socket klientSocket;
	private BufferedReader input;
	private PrintWriter output;
	
	public Serwis(Socket klientSocket, Serwer serwer) {
	    this.serwer = serwer;
	    this.klientSocket = klientSocket;
	    id=serwer.kolejneID();
	}

	void init() throws IOException {
	    Reader reader = new InputStreamReader(klientSocket.getInputStream());
	    input = new BufferedReader(reader);
	    output = new PrintWriter(klientSocket.getOutputStream(), true);
	}
	
	void zakoncz() {
	    try {
	        output.close();
	        input.close();
	        klientSocket.close();
	    } catch (IOException e) {
	    	serwer.wpiszKonsola(e.toString());
	    } finally {
	        output = null;
	        input = null;
	        klientSocket = null;
	    }
	}
	
	public void run() {
	    while (true) {
	        String zapytanie = odbierz();
	        StringTokenizer st = new StringTokenizer(zapytanie);
	        String polecenie = st.nextToken();
	        if (polecenie.equals(ProtokolSieciowy.KE_ZALOGUJ)) {
	            wyslij(ProtokolSieciowy.KE_ZALOGOWALEM);
	            serwer.wpiszKonsola("S"+id+": "+ProtokolSieciowy.KE_ZALOGOWALEM);
	        } 
	        else if (polecenie.equals(ProtokolSieciowy.PARAMETRY_JAKIE)) {
	        	wyslij(ProtokolSieciowy.PARAMETRY_TO+" "+" "+serwer.menager.predkosci()+" "+serwer.menager.iloscZyc()+" "+serwer.menager.punktacja());	     
	        	serwer.wpiszKonsola("S"+id+": "+ProtokolSieciowy.PARAMETRY_TO+" "+" "+serwer.menager.predkosci()+" "+serwer.menager.iloscZyc()+" "+serwer.menager.punktacja());
	        } 
	        else if (polecenie.equals(ProtokolSieciowy.TOP_LISTA_JAKA)) {
	        	wyslij(ProtokolSieciowy.TOP_LISTA_TO + " " + serwer.menager.topLista());
	        	serwer.wpiszKonsola("S"+id+": "+ProtokolSieciowy.TOP_LISTA_TO + " " + serwer.menager.topLista());
	        } 
	        else if (polecenie.equals(ProtokolSieciowy.TOP_ZGLASZAM_WYNIK )) {	
	        	int wynik = Integer.parseInt(st.nextToken());
	            wyslij(ProtokolSieciowy.TOP_AKCEPTUJE + " " + serwer.menager.czyDobryWynik(wynik));
	            serwer.wpiszKonsola("S"+id+": "+ProtokolSieciowy.TOP_AKCEPTUJE + " " + serwer.menager.czyDobryWynik(wynik));
	        } 
	        else if (polecenie.equals(ProtokolSieciowy.TOP_DANE_ZAAKCEPTOWANEGO)) {
	        	String wynik=null;
	        	if (st.hasMoreTokens()) 
	        		wynik = st.nextToken();
	        	String imie=null;
	        	if (st.hasMoreTokens())
	        		imie = st.nextToken();
	        	serwer.menager.dodajPozycja(Integer.parseInt(wynik), imie);
	        }
	        else if (polecenie.equals(ProtokolSieciowy.MAPA_DAJ_POZIOM)) {
	        	int numer=Integer.parseInt(st.nextToken());
	        	wyslij(ProtokolSieciowy.MAPA_DAJE_POZIOM + " "+ serwer.menager.poziom(numer));
	        	serwer.wpiszKonsola("S"+id+": "+ProtokolSieciowy.MAPA_DAJE_POZIOM + " "+ serwer.menager.poziom(numer));
	        }
	        else if (polecenie.equals(ProtokolSieciowy.KE_WYLOGUJ)) {
	        	wyslij(ProtokolSieciowy.KE_WYLOGOWALEM);
	        	serwer.wpiszKonsola("S"+id+": "+ProtokolSieciowy.KE_WYLOGOWALEM);
	        	break;
	        }
	        else if (polecenie.equals(ProtokolSieciowy.KE_PRZYJALEM_KONIEC)) {
	        	serwer.wpiszKonsola("S"+id+": "+ProtokolSieciowy.KE_WYLOGOWALEM);
	        	break;
	        }
	        else if (polecenie.equals(ProtokolSieciowy.NULLCOMMAND)) {
	     
	        }
	    } 
	    serwer.usunKlientSerwis(this);
	}
	
	void wyslij(String command) {
	    output.println(command);
	}
	
	private String odbierz() {
	    try {
	    	String polecenie=input.readLine();
	    	serwer.wpiszKonsola("K"+id+": "+polecenie);
	        return polecenie; 
	    } catch (IOException e) {
	    	serwer.wpiszKonsola(e.toString());
	    }
	    return ProtokolSieciowy.NULLCOMMAND;
	} 
}
