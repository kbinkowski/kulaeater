package serwerkulaeater;
import static serwerkulaeater.stale.Stale.SCIEZKA_PARAMETRY_XML;
import static serwerkulaeater.stale.Stale.SCIEZKA_TOPLISTA_XML;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;


/**Obiekty tej klasy wykonuj� operacje na plikach konfiguracyjnych gry.
 * @author K-E team
 *
 */

public class Menager {
	
	/**Pole reprezentuj�ce predko�� KulaEatera.
	 * 
	 */
	private double predkoscKe;
	/**Pole reprezentuj�ce predko�� wrog�w.
	 * 
	 */
	private double predkoscWrog;
	/**Pole reprezentuj�ce ilo�� �y�.
	 * 
	 */
	private int iloscZyc;
	/**Tablica zawieraj�ca nazwy i wyniki graczy zakwalifikowanych na toplist�.
	 * 
	 */
	private String[] toplista;
	/**Tablica zawieraj�ca kolejne poziomy gry.
	 * 
	 */
	private Poziom[] poziom;
	/**Pole zawieraj�ce informacje o punktacji gry. 
	 * 
	 */
	private String punktacja;
	/**Pole reprezentuj�ce aktualny obiekt klasy Serwer.
	 * 
	 */
	private Serwer serwer;
	
	/**Konstruktor klasy Menager.
	 * @param serwer Obiekt klasy Serwer, do kt�rego nale�y tworzony Menager.
	 */
	public Menager(Serwer serwer){
		this.serwer=serwer;
		wczytajParametry();
		wczytajPoziomy();
		wczytajTopLista();
	}
	
	/**Metoda odpowedzialna za wczytanie parametr�w gry z plik�w konfiguracyjnych. Wo�ana z konstruktora klasy Menager.
	 * 
	 */
	private void wczytajParametry(){
		try{
			Element korzen;
			File xmlPlik = new File(SCIEZKA_PARAMETRY_XML);
			SAXBuilder builder = new SAXBuilder();
			Document xml = (Document) builder.build(xmlPlik);
			korzen = xml.getRootElement();
			if (korzen.getName().equals("parametrygry")){
				Element el;
				// predkosci
				el =   korzen.getChild("predkosc");
				predkoscKe =  Double.parseDouble(el.getAttributeValue("ke")); 
				predkoscWrog =  Double.parseDouble(el.getAttributeValue("wrog")); 
				//ilosc zyc
				el =   korzen.getChild("zycie");
				iloscZyc = Integer.parseInt(el.getAttributeValue("ilosc"));
				//punktacja
				el = korzen.getChild("punktacja");
				punktacja = el.getAttributeValue("kulka")+" "+ el.getAttributeValue("wrog")+" "+ el.getAttributeValue("zycie")+" "+ el.getAttributeValue("czas");
			}
		} catch (JDOMException e) {
			serwer.wpiszKonsola(e.toString());
		} catch (IOException e) {
			serwer.wpiszKonsola(e.toString());
		}
	}
	
	/**Metoda odpowedzialna za wczytanie poziom�w gry z plik�w konfiguracyjnych. Wo�ana z konstruktora klasy Menager.
	 * 
	 */
	private void wczytajPoziomy(){
		poziom = new Poziom[10];
		for (int i=1; i<=10; i++){
			poziom[i-1] = new Poziom(i); 
		}
	}
	
	/**Metoda odpowedzialna za wczytanie toplisty z pliku konfiguracyjnych. Wo�ana z konstruktora klasy Menager.
	 * 
	 */
	private void wczytajTopLista(){
		try{
			toplista = new String[10];
			Element korzen;
			File xmlPlik = new File(SCIEZKA_TOPLISTA_XML);
			SAXBuilder builder = new SAXBuilder();
			Document xml = (Document) builder.build(xmlPlik);
			korzen = xml.getRootElement();
			if (korzen.getName().equals("toplista")){
				int i=0;
				for (Element el : (List<Element>) korzen.getChildren("gracz")){
					toplista[i]=""+el.getText()+" "+el.getAttributeValue("punkty");
					i++;
				}
			}
		} catch (JDOMException e) {
			serwer.wpiszKonsola(e.toString());
		} catch (IOException e) {
			serwer.wpiszKonsola(e.toString());
		}
	}
	
	
	/**Metoda zwracaj�ca pr�dko�ci KulaEatera i wrog�w w postaci Stringu.
	 * @return Pr�dko�ci KulaEatera i wrog�w.
	 */
	synchronized String predkosci() {
		return ""+predkoscKe+" "+predkoscWrog;
	}
	
	
	
	/**Metoda zwracaj�ca ilosc �y� KulaEatera.
	 * @return Ilo�� �y�.
	 */
	synchronized String iloscZyc() {
		return ""+ iloscZyc; 
	}
	
	
	/**Metoda zwracaj�ca topliste i wrog�w w postaci Stringu.
	 * @return Toplista.
	 */
	synchronized String topLista(){
		String odpowiedz="";
		for (int i=0; i<10; i++){
			odpowiedz=odpowiedz+(i+1)+" "+toplista[i]+" ";
		}
		return odpowiedz;
	}
	
	/**Metoda sprawdzaj�ca czy dany podany wynik kwalifikuje si� na topliste.
	 * @param wynik Wynik gracza.
	 * @return Pozycja zakwalifikowanego gracza. Gdy niekwalifikuje si� 0.
	 */
	synchronized int czyDobryWynik(int wynik){
		int i;
		for (i=10; i>0; i--){
			if (Integer.parseInt(toplista[i-1].split(" ")[1])>=wynik) break;
		}
		i=i+1;
		if (i>10) return 0;
		else return i;
	}
		
	/**Metoda dodaj�ca do toplisty gracza, jego imi� i wynik.
	 * @param wynik Wynik gracza.
	 * @param imie Imi� gracza.
	 */
	synchronized void dodajPozycja(int wynik, String imie){
		int i;
		for (i=10; i>0; i--){
			if (Integer.parseInt(toplista[i-1].split(" ")[1])>=wynik) break;
		}
		i=i+1;
		if(i<=10 && i >0){
			for (int j=10; j>i; j--){
				toplista[j-1]=toplista[j-2];
			}
		toplista[i-1]=""+imie+" "+wynik;
		}
	}
	
	/**Metoda zapisuj�ca aktualnie przechowywan� toplist� do pliku konfiguracyjnego.
	 * 
	 */
	synchronized void zapiszToplista(){
		BufferedWriter strumien;
		try {
			strumien = new BufferedWriter(new FileWriter(SCIEZKA_TOPLISTA_XML));
			XMLOutputter builder = new XMLOutputter();
			Element root = new Element("toplista");
			Document xml = new Document(root);
			Element el;
			for (int i=0; i<10; i++){
				el= new Element("gracz");
				el.setAttribute("pozycja",""+(i+1));
				el.setAttribute("punkty",""+toplista[i].split(" ")[1]);
				el.setText(toplista[i].split(" ")[0]);
				root.addContent(el);
			}
			
			builder.output(xml, strumien);
		} catch (IOException e) {
			serwer.wpiszKonsola(e.toString());
		}
	}
	
	/**Metoda zwracaj�ca informacje o poziomie, o numerze podawanym jako parametr, w postaci Stringu.
	 * @param numer Numer poziomu.
	 * @return Poziom w postaci Stringu.
	 */
	synchronized String poziom(int numer){
		if (numer>0 && numer <=10) return ""+ poziom[numer-1];
		else return "ERROR";
	}
	
	/**Metoda zwracaj�ca punktacje w postaci Stringu, kolejno informacja o:
	 * punktach za kulke, za zjedzonego wroga, za niewykorzystane �ycie, 
	 * za niewykorzystane jednostki czasu. 
	 * @return Punktacja.
	 */
	synchronized String punktacja() {
		return punktacja;
	}
	
	
}
