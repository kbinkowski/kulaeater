package serwerkulaeater;
import java.awt.TextArea;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.JTextField;

import serwerkulaeater.stale.ProtokolSieciowy;


/**Klasa reprezentuj�ca serwer gry KulaEater.
 * @author K-E team
 *
 */
public class Serwer implements Runnable {
	
	/**Gniazdo Serwera sieciowego.
	 * 
	 */
	private ServerSocket serverSocket;
    /**Wektor obiekt�w klasy Serwis obs�uguj�cych konkretnych klient�w. 
     * 
     */
    private Vector<Serwis> clients = new Vector<Serwis>();
    /**Pole reprezentuj�ce obiekt klasy Menager.
     * 
     */
    Menager menager;
    /**Pole s�u��ce do prezentacji komunikat�w przesy�anych.
     * 
     */
    private TextArea konsola;
    /**Pole s�u��ce do prezentacji ilo�ci klient�w.
     * 
     */
    private JTextField ilosc;
    /**Pole s�u��ce do przydzielania ID klientowi.
     * 
     */
    private int ostatnieID = 0;
    /**Pole reprezentuj�ce ilo�� aktywnych klient�w.
     * 
     */
    private int iloscUzytkownikow = 0;
    /**Pole wskazuj�ce na stan pracy Serwera. Gdy false - w�tek serwera ko�czy dzia�anie.
     * 
     */
    private boolean pracuj;
    
    /**Konstruktor klasy Serwer.
     * @param port Adres portu.
     * @param konsola Konsola wy�wietlaj�ca komunikaty.
     * @param ilosc Pole prezentuj�ce ilo�� u�ytkownik�w.
     * @throws IOException
     */
    public Serwer(int port, TextArea konsola, JTextField ilosc) throws IOException{
    	this.ilosc=ilosc;
	    serverSocket = new ServerSocket(port);
	    menager = new Menager(this);
	    pracuj=true;
	    new Thread(this).start();
	    this.konsola=konsola;
	    this.konsola.append("Uruchomiony na porcie o numerze: " +port+". "+"Adres: "+InetAddress.getByName("localhost")+"\n");
    }
    
    /**Metoda wpisuj�ca do konsoli okna aplikacji serwera gry KulaEater komunikatu podawanego jako parametr.
     * @param s Komunikat do wy�wietlenia.
     */
    public synchronized void wpiszKonsola(String s){
    	konsola.append(s+"\n");
    }
    /**Metoda przydzielaj�ca ID.
     * @return ID klienta.
     */
    public synchronized int kolejneID(){
    	return ++ostatnieID;
    }
    /**Metoda zwracaj�ca ilo�� u�ytkownik�w.
     * @return Ilo�� u�ytkownik�w. 
     */
    public int dajIloscUzytkownikow(){
    	return iloscUzytkownikow;
    }
    /**Metoda aktualizuj�ca dane przechowywane po stronie serwera (toplista).
     * 
     */
    public void uaktualnijDane(){
    	menager.zapiszToplista();
    }
    
    
    public void run() {
        while (pracuj){
                Socket klientSocket;
				try {
					klientSocket = serverSocket.accept();
					Serwis klientSerwis = new Serwis(klientSocket, this);
					dodajKlientSerwis(klientSerwis);
				}  catch (IOException e) {
			    	wpiszKonsola(e.toString());
				}	
        }
    }
    
    /**Metoda s�u��ca do zako�czenia pracy serwera. Wysy�aj�ca do klient�w komunikat o ch�ci zako�czenia.
     * 
     */
    public void zakoncz(){
    	wyslij(ProtokolSieciowy.KE_KONCZE);
    	wpiszKonsola("Serwer: "+ProtokolSieciowy.KE_KONCZE);
    	int i=0;
        while (clients.size() != 0)
            try {
                Thread.sleep(100);
                if(++i==100) break;
            } catch (InterruptedException e) {
    	    	wpiszKonsola(e.toString());
        }
        try {
        	pracuj=false;
			serverSocket.close();
		} catch (IOException e) {
			wpiszKonsola(e.toString());
		} 
        ilosc.setText("Zalogowanych u�ytkownik�w: 0");
    }
    
    /**Metoda dodaj�ca klienta. 
     * @param klientSerwis Serwis odpowiedzialny za obs�ug� konkretnego klienta.
     * @throws IOException
     */
    synchronized void dodajKlientSerwis(Serwis klientSerwis)
            throws IOException {
		        klientSerwis.init();
		        clients.addElement(klientSerwis);
		        new Thread(klientSerwis).start();
		        ilosc.setText("Zalogowanych u�ytkownik�w: "+(++iloscUzytkownikow));
    }

    /**Metoda usuwaj�ca klienta. 
     * @param klientSerwis Serwis odpowiedzialny za obs�ug� konkretnego klienta.
     */
    synchronized void usunKlientSerwis(Serwis klientSerwis) {
        clients.removeElement(klientSerwis);
        klientSerwis.zakoncz();
        ilosc.setText("Zalogowanych u�ytkownik�w: "+(--iloscUzytkownikow));
    }

    /**Metoda wysy�aj�ca do wszystkich klient�w komunikat.
     * @param msg Komunikat.
     */
    synchronized void wyslij(String msg) {
        Enumeration<Serwis> e = clients.elements();
        while (e.hasMoreElements())
            ((Serwis) e.nextElement()).wyslij(msg);
    }

}
