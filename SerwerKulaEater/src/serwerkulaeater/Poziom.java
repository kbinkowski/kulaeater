package serwerkulaeater;

import static serwerkulaeater.stale.Stale.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;



/**Obiekty tej klasy zawierają informacje o poziomie gry.
 * @author K-E team
 *
 */
public class Poziom {
	/**Pole reprezentujące numer poziomu.
	 * 
	 */
	private int numer;
	/**Pole reprezentujące czas przeznaczony na poziom.
	 * 
	 */
	private int czas;
	/**Pole reprezentujące pozycję w poziomie KulaEatera.
	 * 
	 */
	private int xKe;
	/**Pole reprezentujące pozycję w pionie KulaEatera.
	 * 
	 */
	private int yKe;
	/**Pole reprezentujące pozycję w poziomie wroga.
	 * 
	 */
	private int xWrog;
	/**Pole reprezentujące pozycję w pionie Wroga.
	 * 
	 */
	private int yWrog;
	/**Pole reprezentujące ilosc wrogów.
	 * 
	 */
	private int iloscWrog;
	/**Pole reprezentujące rozkład pól poziomu.
	 * 
	 */
	private String mapa;
	
	
	/**Konstruktor klasy Poziom.
	 * @param numer Numer poziomu;
	 */
	public Poziom(int numer){
		this.numer = numer;
		String tekst;
		try {
			BufferedReader strumien = new BufferedReader(new FileReader(SCIEZKA_POZIOM+numer+".pgm"));
			strumien.readLine(); // format pliku
			tekst = strumien.readLine();
			czas = Integer.parseInt(tekst.split(" ")[2]);
			tekst = strumien.readLine(); // pozycja kula eatera
			xKe = Integer.parseInt(tekst.split(" ")[2]);
			yKe = Integer.parseInt(tekst.split(" ")[3]);
			tekst = strumien.readLine(); // info o wrogach
			iloscWrog = Integer.parseInt(tekst.split(" ")[2]);
			xWrog = Integer.parseInt(tekst.split(" ")[3]);
			yWrog = Integer.parseInt(tekst.split(" ")[4]);
			strumien.readLine(); // rozmiar
			strumien.readLine(); // zakres 
			mapa="";
			while((tekst=strumien.readLine())!=null){
				mapa= mapa + " " + tekst;
			}
			strumien.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}		
	}


	/**Metoda zamieniająca obiekt klasy Poziom w String.Informacje o poziomie, kolejno: numer, czas, pozycja KulaEatera w poziomie i w pionie, ilosc wrogów, pozycja wrogów w poziomie i w pionie, układ poziomu.
	 */
	public String toString() {
		return ""+numer+" "+czas+" "+xKe+" "+yKe+" "+iloscWrog+" "+xWrog+" "+yWrog+" "+mapa;
	}
	
}
