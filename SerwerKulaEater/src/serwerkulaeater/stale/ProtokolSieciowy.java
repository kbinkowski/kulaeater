package serwerkulaeater.stale;

public interface ProtokolSieciowy {
	String KE_ZALOGUJ = "ke_zaloguj";

    String KE_ZALOGOWALEM = "ke_zalogowalem";

    String PARAMETRY_JAKIE = "parametry_jakie";
    
    String PARAMETRY_TO = "parametry_to";

    String TOP_LISTA_JAKA = "top_lista_jaka";
    
    String TOP_LISTA_TO = "top_lista_to";

    String TOP_ZGLASZAM_WYNIK = "top_zglaszam_wynik";

    String TOP_AKCEPTUJE = "top_akceptuje";

    String TOP_DANE_ZAAKCEPTOWANEGO = "top_dane_zaakceptowanego";

    String MAPA_DAJ_POZIOM = "mapa_daj_poziom";

    String MAPA_DAJE_POZIOM = "mapa_daje_poziom";

    String KE_WYLOGUJ = "ke_wyloguj";
    
    String KE_WYLOGOWALEM = "ke_wylogowalem ";
    
    String KE_KONCZE = "ke_koncze";
    
    String KE_PRZYJALEM_KONIEC = "ke_przyjalem_koniec";

    String NULLCOMMAND = "nullcommand";
}
