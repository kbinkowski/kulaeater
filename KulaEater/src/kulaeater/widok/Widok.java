package kulaeater.widok;


import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.*;

import javax.swing.*;

import static kulaeater.stale.Stale.*;
import kulaeater.widok.dzwiek.MenadzerDzwieku;
import kulaeater.widok.zdarzenia.*;
import kulaeater.stale.*;
/**Klasa odpowiedzialna za kontekst graficzny gry, sposób prezentacji gry.
 * @author K-E team
 *
 */
public class Widok {
	/**Okno gry.
	 * 
	 */
	private JFrame okno;
	/**Panel główny.
	 * 
	 */
	private JPanel glownyPanel;
	/**Panel ToplistaWidok.
	 * 
	 */
	private ToplistaWidok toplista;
	/**Panel AutorzyWidok.
	 * 
	 */
	private AutorzyWidok autorzy;
	/**Panel Menu.
	 * 
	 */
	private Menu menu;
	/**Panel ArenaWidok.
	 * 
	 */
	private ArenaWidok arena;
	
	/**Menadzer dzwięku.
	 * 
	 */
	private MenadzerDzwieku dzwiek;
	
	/**Referencja na kolejke zdarzeń. Klasa Widok posiada metody umieszczające w kolejce zdarzenia różnego typu. 
	 * 
	 */
	final BlockingQueue<Event> kolejka;
	
	
	/**Konstruktor klasy Widok.
	 * @param kolejka
	 */
	public Widok(BlockingQueue<Event> kolejka){
		this.kolejka = kolejka;
		
		
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				init();
			}
		});
	}
	
	/**Metoda inicjująca okno gry.
	 * 
	 */
	private void init () {
		dzwiek = new MenadzerDzwieku();
		menu=new Menu(this);
		toplista=new ToplistaWidok(this);
		autorzy=new AutorzyWidok(this);
		arena=new ArenaWidok(this);
		glownyPanel = new JPanel();
		glownyPanel.setLayout(new GridLayout(1,1));
		glownyPanel.setPreferredSize(new Dimension(SZEROKOSC_POLA_GRY,WYSOKOSC_PANELU_KONTROLNEGO+WYSOKOSC_POLA_GRY));
        //glownyPanel.add(arena); //TODO: zmien na menu

		ustawWidocznyPanel(menu);
        okno = new JFrame("The KulaEater!");
        okno.add(glownyPanel); 
        okno.pack();
        okno.setVisible(true);
        okno.addKeyListener(new Klawiatura(this));
        okno.setFocusable(true);
        okno.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
            	kolejka.add(new ZakonczEvent());
            }
         }); 
	}

	/**Metoda przełączająca wyświetlany stan gry.
	 * @param panel
	 */
	private void ustawWidocznyPanel(final JPanel panel){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				glownyPanel.removeAll();
				glownyPanel.add(panel);
				glownyPanel.revalidate();
				glownyPanel.repaint(); 
			}
		});
	}
	
	//inne metody widoku bla bla
	
	/**Metoda wyświetlająca Toplistę.
	 * @param wyniki Mapa zawierająca informacje o najlepszych wynikach.
	 */
	public void wyswietlTopliste(final Map<Integer, String> wyniki) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				toplista.aktualizujListe(wyniki);
				ustawWidocznyPanel(toplista);
			}
		});
	}
	/**Metoda wyświetlająca informacje o autorach gry.
	 * @param tekst
	 */
	public void wyswietlAutorow(final String tekst){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				autorzy.aktualizujTekst(tekst);
				ustawWidocznyPanel(autorzy);
			}
		});
	}
	/**Metoda powracająca do menu gry.
	 * 
	 */
	public void wrocDoMenu(){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				ustawWidocznyPanel(menu);
			}
		});
	}
	/**Metoda aktualizująca panel stanu gry.
	 * @param zycia Ilość żyć.
	 * @param poziom Tekst zawierający informacje o poziomie.
	 * @param bonus Tekst zawierający informacje o bonusie.
	 * @param punkty Ilość punktów.
	 * @param czas Czas przeznaczony na dokończenie poziomu.
	 */
	public void aktualizujKontrolki(final int zycia, final String poziom, final String bonus, final int punkty, final int czas){
		

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				arena.odswiezKontrolki(zycia, poziom, bonus, punkty, czas);
			}
		});
	}
	/**Metoda aktualizująca pole gry.
	 * @param uklad Układ mapy.
	 * @param pozPacman Pozycja KulaEatera.
	 * @param zwrot Zwrot KulaEatera.
	 * @param pozWrog Pozycje wrogów.
	 * @param zaslona Parametr ustawiający zasłonę ograniczającą widoczność.
	 */
	public void aktualizujPlansze(final RodzajPola[][] uklad, final double[] pozPacman, final Kierunek zwrot, final LinkedList<double[]>pozWrog, final boolean zaslona){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				arena.odwiezPlansze(uklad, pozPacman, zwrot, pozWrog,zaslona);
			}
		});
	}
	
	/**Metoda ustawia widok (ikonę przycisku w menu) zgodnie z trybem online/offline
	 * @param tak tak=true tryb onlina w przeciwnym wypadku offline
	 */
	public void wyswietlTrybOnline(final boolean tak){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				menu.aktualizujPrzyciskOnline(tak?SCIEZKA_MENU_ONLINE:SCIEZKA_MENU_OFFLINE);
			}
		});
	}
	
	/**Metoda wyświetlająca okno z poleceniem podania adresu serwera.
	 * @return Adres serwera.
	 */
	public String zapytajAdresSerwera(){
		//SwingUtilities.invokeLater(new Runnable() {
			//@Override
			//public void run() {
				return JOptionPane.showInputDialog(null,"Podaj adres : ","localhost:11135");
			//}
		//});
	}
	/**Metoda wyświetlająca komunikat na ekran w postaci nowego okna.
	 * @param tekst Komunikat wyświetlany.
	 */
	public void wyswietlKomunikat(final String tekst){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JOptionPane.showMessageDialog(null, tekst);
			}
		});
	}
	/**Metoda przełączająca stan gry na rozgrywkę.
	 * 
	 */
	public void wyswietlRozgrywke() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				glownyPanel.removeAll();
		        glownyPanel.add(arena);
			}
		});
	}
	/**Metoda wyśweitlająca okno z zapytaniem o imie gracza.
	 * @return Imie gracza.
	 */
	public String zapytajImieGracza(){
	//SwingUtilities.invokeLater(new Runnable() {
		//@Override
		//public void run() {
			return JOptionPane.showInputDialog(null,"Podaj imię : ");
		//}
	//});
	}
	
	public void startDzwiekKulka(){
		dzwiek.zjedzonaKula();
	}
	
	public void startDzwiekSmierc(){
		dzwiek.zabityKulaEater();
	}
}
