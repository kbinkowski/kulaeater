package kulaeater.widok;

import static kulaeater.stale.Stale.*;
import kulaeater.model.zdarzenia.BladEvent;
import kulaeater.widok.zdarzenia.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;



/**Klasa reprezentująca panel wyboru stanu gry (menu).
 * @author K-E team
 *
 */
public class Menu extends JPanel{
	/**Pole reprezentujące obraz wykorzystywany w prezentacji menu gry.
	 * 
	 */
	private Image tlo,tlo2;
	/**Pole reprezentujące obraz wykorzystywany w prezentacji menu gry.
	 * 
	 */
	private final Przycisk grajPrzycisk, toplistaPrzycisk, autorzyPrzycisk, koniecPrzycisk, onlinePrzycisk;
	/**Referencja na widok do którego należy.
	 * 
	 */
	private final Widok mojWidok;
   
	/**Konstruktor klasy Menu.
	 * @param widok Referencja ma Widok do którego należy tworzony obiekt.
	 */
	Menu(Widok widok){
		mojWidok=widok;
		try{
	        tlo=ImageIO.read(new File(SCIEZKA_MENU_TLO)); 
		}
		catch(IOException e){ 
			mojWidok.kolejka.add(new BladEvent(e.toString()));	
		}
		//tworzenie przyciskow
		grajPrzycisk=new Przycisk(SCIEZKA_MENU_GRAJ);
		toplistaPrzycisk=new Przycisk(SCIEZKA_MENU_TOPLISTA);
		autorzyPrzycisk=new Przycisk(SCIEZKA_MENU_AUTORZY);
		koniecPrzycisk=new Przycisk(SCIEZKA_MENU_KONIEC);
		onlinePrzycisk=new Przycisk(SCIEZKA_MENU_OFFLINE);

		//dodanie zdarzen do przyciskow
		grajPrzycisk.addActionListener(new ActionListener() {
			   public void actionPerformed(ActionEvent e){
				   try{
					   mojWidok.kolejka.put(new GrajEvent());
				   } 
				   catch (InterruptedException exc){
					   mojWidok.kolejka.add(new BladEvent(exc.toString()));	
				   }
			   }
		});
		
		toplistaPrzycisk.addActionListener(new ActionListener() {
			   public void actionPerformed(ActionEvent e){
				   try{
					   mojWidok.kolejka.put(new PokazToplisteEvent());
				   } 
				   catch (InterruptedException exc){
					   mojWidok.kolejka.add(new BladEvent(exc.toString()));	
				   }
			   }
		});
		
		autorzyPrzycisk.addActionListener(new ActionListener() {
			   public void actionPerformed(ActionEvent e){
				   try{
					   mojWidok.kolejka.put(new PokazAutorowEvent());
				   } 
				   catch (InterruptedException exc){
					   mojWidok.kolejka.add(new BladEvent(exc.toString()));	
				   }
			   }
		});
		
		koniecPrzycisk.addActionListener(new ActionListener() {
			   public void actionPerformed(ActionEvent e){
				   try{
					   mojWidok.kolejka.put(new ZakonczEvent());
				   } 
				   catch (InterruptedException exc){
					   mojWidok.kolejka.add(new BladEvent(exc.toString()));	
				   }
			   }
		});
		
		onlinePrzycisk.addActionListener(new ActionListener() {
			   public void actionPerformed(ActionEvent e){
				   try{
					   if(onlinePrzycisk.tekst().equals(SCIEZKA_MENU_ONLINE))
						   mojWidok.kolejka.put(new WidokOnlineEvent(false));
					   else if(onlinePrzycisk.tekst().equals(SCIEZKA_MENU_OFFLINE))
						   mojWidok.kolejka.put(new WidokOnlineEvent(true));
				   } 
				   catch (InterruptedException exc){
					   mojWidok.kolejka.add(new BladEvent(exc.toString()));	
				   }
			   }
		});
		
		//wyswietlenie przyciskow
		setLayout(new GridLayout(1,5));
		JPanel przyciski= new JPanel();
		przyciski.setLayout(new GridLayout(11,1));
		przyciski.setBackground(Color.BLACK);
		przyciski.add(new JLabel(""));
		przyciski.add(grajPrzycisk);
		przyciski.add(new JLabel(""));
		przyciski.add(toplistaPrzycisk);
		przyciski.add(new JLabel(""));
		przyciski.add(autorzyPrzycisk);
		przyciski.add(new JLabel(""));
		przyciski.add(koniecPrzycisk);
		przyciski.add(new JLabel(""));
		przyciski.add(onlinePrzycisk);
		przyciski.add(new JLabel(""));
		add(new JLabel(""));
		add(new JLabel(""));
		add(przyciski);

   }
	
	void aktualizujPrzyciskOnline(String s){
		onlinePrzycisk.ustawPrzycisk(s);
	}
   
   protected void paintComponent(Graphics g){
        super.paintComponent(g);
        tlo2=Skalowanie.przeskalujObraz(tlo,getWidth(),getHeight());
        g.drawImage(tlo2,0,0,this);
    } 
   
   
}
