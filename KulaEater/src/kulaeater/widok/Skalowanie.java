package kulaeater.widok;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

/**Klasa odpowiedzialna za skalowanie obraz�w.
 * @author K-E team
 *
 */
public class Skalowanie {
        /**Metoda skaluj�ca obraz.
         * @param image Obiekt obrazka przeznaczonego do skalowania.
         * @param width Szeroko�� nowego obrazka.
         * @param height Wysoko�� nowego obrazka.
         * @return Przeskalowany obraz.
         */
        public static BufferedImage przeskalujObraz(final Image image, int width, int height) {	
        	width = width>0?width:1;
        	height = height>0?height:1;
		    final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		    final Graphics2D graphics2D = bufferedImage.createGraphics();
		    graphics2D.setComposite(AlphaComposite.Src);
		    graphics2D.drawImage(image, 0, 0, width, height, null);
		    graphics2D.dispose();
		    return bufferedImage;
        }
        
}
