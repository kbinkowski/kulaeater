package kulaeater.widok.dzwiek;

public class MenadzerDzwieku {
	
	private Dzwiek zabityKulaEater;
	private Dzwiek zjedzonaKula;
	//private Dzwiek tlo;
	
	public MenadzerDzwieku() {
		zabityKulaEater=new Dzwiek("music/smierc.wav");
		zjedzonaKula   =new Dzwiek("music/kulka.wav");
		//tlo		   =new Dzwiek("music/tlo.wav");	
	}
	
	public void zjedzonaKula(){
		zjedzonaKula.run();
	}
	
	public void zabityKulaEater(){
		zabityKulaEater.run();
	}
}
