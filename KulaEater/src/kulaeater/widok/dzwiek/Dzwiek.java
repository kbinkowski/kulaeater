package kulaeater.widok.dzwiek;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.MalformedURLException;
import java.net.URL;

public class Dzwiek extends Thread {

	AudioClip plik;
	boolean petla;
	
	public Dzwiek(String url) {
		this.petla=false;
		try {
			plik=Applet.newAudioClip(new URL("file:"+url));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Dzwiek(String url, boolean petla) {
		this.petla=petla;
		try {
			plik=Applet.newAudioClip(new URL("file:"+url));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start();
	}
	
	public void run() {
		if (!petla){
			plik.play();
		}
		else{
			plik.loop();
		}
	}
	
}
