package kulaeater.widok;

import static kulaeater.stale.Stale.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import kulaeater.model.zdarzenia.BladEvent;

/**Klasa reprezentuj�c panel stanu gry.
 * @author K-E team
 *
 */
public class KontrolkiWidok extends JPanel {

	private static final long serialVersionUID = 1L;

	/**Referencja na widok do kt�rego nale�y.
	 * 
	 */
	private final Widok mojWidok;
	/**Tekst wy�wietlany w panelu stanu gry.
	 * 
	 */
	private final String strCzas = "Czas:";
	/**Tekst wy�wietlany w panelu stanu gry.
	 * 
	 */
	private final String strPkt = "Punkty:";
	/**Pole zawieraj�ce informacje na temat ilo�ci �y�.
	 * 
	 */
	private int zyciaTmp =3;
	/**Tekst informuj�cy o zebranym bonusie.
	 * 
	 */
	private String strBonus="";
	/**Tekst informuj�cy o aktualnym poziomie.
	 * 
	 */
	private String strPoziom="";
	/**Tekst informuj�cy o czasie przeznaczonym na poziom.
	 * 
	 */
	private String strCzasLicz="";
	/**Tekst informuj�cy o aktualnym wyniku gracza.
	 * 
	 */
	private String strPktLicz="";
	
	/**Pole reprezentuj�ce obraz wykorzystywany do prezentacji stanu gry. 
	 * 
	 */
	private Image pngZycie, pngZycieTmp;	
	/**Czcionka wykorzytywana przy prezentacji stanu gry.
	 * 
	 */
	private final Font malaCzcionka = new Font("SansSerif", Font.PLAIN, 12);
	/**Czcionka wykorzytywana przy prezentacji stanu gry.
	 * 
	 */
	private final Font duzaCzcionka = new Font("Monotype", Font.PLAIN, 30);
	/**Czcionka wykorzytywana przy prezentacji stanu gry.
	 * 
	 */
	private final Font sredniaCzcionka = new Font("SansSerif", Font.PLAIN, 20);
	/**Kolor wykorzytywany przy prezentacji stanu gry.
	 * 
	 */
	private final Color zwyklyKolor = Color.white;
	/**Kolor wykorzytywany przy prezentacji stanu gry.
	 * 
	 */
	private final Color bonusowyKolor = Color.green;
	
	/**Konstruktor klasy KontrolkiWidok.
	 * @param widok
	 */
	public KontrolkiWidok(Widok widok) {

		mojWidok = widok;
		setPreferredSize(new Dimension(SZEROKOSC_POLA_GRY, WYSOKOSC_PANELU_KONTROLNEGO));
		setBackground(Color.black);
		try {
			pngZycie=ImageIO.read(new File (SCIEZKA_PANEL_KONTROLNY_ZYCIE));
		} catch (IOException e) {
			mojWidok.kolejka.add(new BladEvent(e.toString()));	
		}

	}
	
	/**Metoda nastawiaj�ca przechowywane parametry na aktualne w celu prezentacji na ekranie.
	 * @param zycia Ilo�� �y�.
	 * @param poziom Informacja o poziomie.
	 * @param bonus Informacja o bonusie. 
	 * @param punkty Ilo�� punkt�w.
	 * @param czas Czas na doko�czenie poziomu.
	 */
	public void odswiez(int zycia, String poziom, String bonus, int punkty, int czas){
		zyciaTmp = zycia;
		strBonus = bonus;
		strPoziom = poziom;
		strCzasLicz = ""+czas;
		strPktLicz = ""+punkty;
		revalidate();
		repaint();
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int sze = getWidth();
		
		for (int z=0; z<zyciaTmp; z++){
			g.drawImage(pngZycie,10+40*z,10,this);
		}
		
		g.setColor(zwyklyKolor);
		g.setFont(malaCzcionka);
		g.drawString(strCzas,sze-60, 12);
		g.drawString(strPkt,sze-160, 12);
		
		g.setFont(duzaCzcionka);
		g.drawString(strCzasLicz,sze-70, 47);
		g.drawString(strPktLicz,sze-170, 47);	
	
		g.setFont(sredniaCzcionka);
		g.drawString(strPoziom,sze/2-100, 45);
		
		g.setColor(bonusowyKolor);
		g.drawString(strBonus,sze/2-100, 22);	
		

	}
}
