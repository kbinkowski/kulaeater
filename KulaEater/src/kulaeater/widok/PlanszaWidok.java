package kulaeater.widok;

import static kulaeater.stale.Stale.*;
import static kulaeater.widok.Skalowanie.przeskalujObraz;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.imageio.ImageIO;
import javax.swing.*;

import kulaeater.model.zdarzenia.BladEvent;
import kulaeater.stale.Kierunek;
import kulaeater.stale.RodzajPola;

/**Klasa reprezentuj�ca pole gry.
 * @author K-E team
 *
 */
public class PlanszaWidok extends JPanel {
	/**Referencja na widok do kt�rego nale�y.
	 * 
	 */
	private final Widok mojWidok;
	/**Macie� przechowywuj�ca informacje na temat uk�adu mapy.
	 * 
	 */
	private RodzajPola[][] tmpPlansza;
	/**Pozycja KulaEatera
	 * 
	 */
	private double[] tmpPozPacman;
	/**Pozycje wrog�w.
	 * 
	 */
	private LinkedList<double[]> tmpPozWrog;
	/**Pole informuj�ce czy ustawiona jest zas�ona ograniczaj�ca widoczno�c.
	 * 
	 */
	private boolean zaslona;
	/**Obraz wykorzystywany w prezentacji pola gry.
	 * 
	 */
	private Image pngMur, pngMurTmp;
	/**Obraz wykorzystywany w prezentacji pola gry.
	 * 
	 */
	private Image pngKulka, pngKulkaTmp;
	/**Obraz wykorzystywany w prezentacji pola gry.
	 * 
	 */
	private Image pngBonusKulka, pngBonusKulkaTmp;
	/**Obraz wykorzystywany w prezentacji pola gry.
	 * 
	 */
	private Image pngDroga, pngDrogaTmp;
	/**Obraz wykorzystywany w prezentacji pola gry.
	 * 
	 */
	private Image WizerunekPacmanLewo, WizerunekPacmanPrawo, WizerunekPacmanGora, WizerunekPacmanDol, WizerunekPacman, WizerunekPacmanTmp;
	/**Obraz wykorzystywany w prezentacji pola gry.
	 * 
	 */
	private Image pngZaslona, pngZaslonaTmp;
	/**Obraz wykorzystywany w prezentacji pola gry.
	 * 
	 */
	private Image pngWizerunekDuch, pngWizerunekDuchTmp;
	/** Zwrot KulaEatera.
	 * 
	 */
	private Kierunek zwrot;
	
	/**Konstruktor klasy PlanszaWidok.
	 * @param widok Referencja na Widok, do k�rego nale�y tworzony obiekt.
	 */
	PlanszaWidok(Widok widok){

		mojWidok = widok;
		setBackground(Color.BLACK);
		try {
			pngMur=ImageIO.read(new File (SCIEZKA_OKNO_PLANSZY_MUR));
			pngKulka=ImageIO.read(new File (SCIEZKA_OKNO_PLANSZY_KULKA));
			pngBonusKulka=ImageIO.read(new File (SCIEZKA_OKNO_PLANSZY_BONUSOWA_KULKA));
			pngDroga=ImageIO.read(new File (SCIEZKA_OKNO_PLANSZY_DROGA));
			pngWizerunekDuch=ImageIO.read(new File (SCIEZKA_OKNO_PLANSZY_WROG));
			WizerunekPacmanPrawo=ImageIO.read(new File (PACMAN_PRAWO));
			WizerunekPacmanLewo=ImageIO.read(new File (PACMAN_LEWO));
			WizerunekPacmanGora=ImageIO.read(new File (PACMAN_GORA));
			WizerunekPacmanDol=ImageIO.read(new File (PACMAN_DOL));
			pngZaslona=ImageIO.read(new File (SCIEZKA_OKNO_PLANSZY_ZASLONA));
		} 
		catch (IOException e) {
			mojWidok.kolejka.add(new BladEvent(e.toString()));	
		}
	}
	/**Metoda od�wie�aj�ca pole gry.
	 * @param uklad Uk�ad mapy.
	 * @param pozPacman Pozycja KulaEatera.
	 * @param zwrotPacman Zwrot KulaEatera.
	 * @param pozWrog Pozycje wrog�w.
	 * @param zaslona Parametr ustawiaj�cy zas�on� ograniczaj�c� widoczno��.
	 */
	synchronized void odswiez(RodzajPola[][] uklad, double[] pozPacman, Kierunek zwrotPacman, LinkedList<double[]> pozWrog, boolean zaslona){
		zwrot=zwrotPacman;
		tmpPlansza=uklad;
		tmpPozPacman=pozPacman;
		tmpPozWrog=pozWrog;
		this.zaslona=zaslona;
		revalidate();
		repaint();
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(tmpPlansza!=null){
			
			int sze = getWidth();
			int wys = getHeight();
			
				
			int szeKafla=(int)sze/KAFLE_NA_SZER;
			int wysKafla=(int)wys/KAFLE_NA_WYS;
	
			int przesunWysrPoziom = (int)((sze-(KAFLE_NA_SZER*szeKafla))/2);
			int przesunWysrPion = (int)((wys-(KAFLE_NA_WYS*wysKafla))/2);
			pngMurTmp = przeskalujObraz(pngMur, szeKafla, wysKafla);
			pngKulkaTmp =przeskalujObraz(pngKulka, szeKafla, wysKafla);
			pngBonusKulkaTmp= przeskalujObraz(pngBonusKulka, szeKafla, wysKafla);
			pngDrogaTmp= przeskalujObraz(pngDroga, szeKafla, wysKafla);
			pngWizerunekDuchTmp = przeskalujObraz(pngWizerunekDuch, szeKafla, wysKafla);
			pngZaslonaTmp = przeskalujObraz(pngZaslona, szeKafla*KAFLE_NA_SZER*2, wysKafla*KAFLE_NA_WYS*2);
		
			for(int i=0; i<(int)KAFLE_NA_WYS;i++){
				for(int j=0; j<(int)KAFLE_NA_SZER;j++){
					switch(tmpPlansza[i][j]){
					case MUR:
						g.drawImage(pngMurTmp,przesunWysrPoziom+j*szeKafla,przesunWysrPion+i*wysKafla,this);
						break;
					case PUSTKA:
						g.drawImage(pngDrogaTmp,przesunWysrPoziom+j*szeKafla,przesunWysrPion+i*wysKafla,this);
						break;
					case KULKOWE:
						g.drawImage(pngKulkaTmp,przesunWysrPoziom+j*szeKafla,przesunWysrPion+i*wysKafla,this);
						break;
					default:
						g.drawImage(pngBonusKulkaTmp,przesunWysrPoziom+j*szeKafla,przesunWysrPion+i*wysKafla,this);
					}
				}
			}
			
			for (double[] w : tmpPozWrog){
				g.drawImage(pngWizerunekDuchTmp, przesunWysrPoziom+(int)(w[0]*szeKafla), przesunWysrPion+(int)(w[1]*wysKafla), this);
			}
			
			if(zaslona) g.drawImage(pngZaslonaTmp,przesunWysrPoziom+(int)(tmpPozPacman[0]*szeKafla)-szeKafla*KAFLE_NA_SZER, przesunWysrPion+(int)(tmpPozPacman[1]*wysKafla)-wysKafla*KAFLE_NA_WYS,this);
			 
		
	
		    
		     switch (zwrot){
			 case GORA:
				 WizerunekPacman = WizerunekPacmanGora;
				 break;
			 case LEWO:
				 WizerunekPacman = WizerunekPacmanLewo;
				  break;
			 case DOL:
				 WizerunekPacman = WizerunekPacmanDol;
				  break;
			 default:
				 WizerunekPacman = WizerunekPacmanPrawo;
				  break;
			 }
		     WizerunekPacmanTmp = przeskalujObraz(WizerunekPacman, szeKafla, wysKafla);
		     g.drawImage(WizerunekPacmanTmp,przesunWysrPoziom+(int)(tmpPozPacman[0]*szeKafla),przesunWysrPion+(int)(tmpPozPacman[1]*wysKafla),this); 
		}
	}
}

