package kulaeater.widok;


import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;

/**Klasa odpowiadająca za prezentuje autorów gry.
 * @author K-E team
 *
 */
public class AutorzyWidok extends JPanel {

	
	private static final long serialVersionUID = 1L;
	
	/**Pole zawierające tekst informujący o autorach.
	 * 
	 */
	private final JLabel autorzy; 
	
	/**Konstruktor klasy AutorzyWidok.
	 * 
	 */
	AutorzyWidok(Widok widok){
		autorzy=new JLabel();
		setBackground(Color.BLACK);
        add(autorzy, BorderLayout.CENTER);
	}
	
	/**Metoda aktualizująca pole autorzy.
	 * @param txt Tekst zawierający informacje o autorach.
	 */
	void aktualizujTekst(String txt){
		autorzy.setText(txt);
	}
}
