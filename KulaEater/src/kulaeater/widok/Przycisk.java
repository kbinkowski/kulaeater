package kulaeater.widok;


import java.awt.Graphics;

import static kulaeater.widok.Skalowanie.*;

import java.awt.Image;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**Klasa reprezentuj�ca przycisk.
 * @author K-E team
 *
 */
public class Przycisk extends JButton implements ImageObserver {
	/**Obraz wykorzystywany w prezentacji przycisku.
	 * 
	 */
    private Image image, image2;
    /**�cie�ka do pliku obrazkowego.
     * 
     */
    private String tekst;
    /**Konstruktor klasy Przycisk.
     * @param �cie�ka do pliku obrazkowego.
     */
    Przycisk(String s){
        try{
			tekst =s;
           image=ImageIO.read(new File(s)); 
           setBorderPainted(false);
        }
        catch(IOException e){}        
    }
    /**Metoda zwracaj�ca �cie�k� do pliku obrazkowego.
     * @return �cie�ka do pliku obrazkowego.
     */
    public String tekst(){
        return tekst;
    }
    /**Metoda aktualizuj�ca obraz przycisku.
     * @param s �cie�ka do pliku obrazkowego.
     */
    public void ustawPrzycisk(String s){
    	try {
    		tekst=s;
			image=ImageIO.read(new File(s));
		} catch (IOException e) {}
    	repaint();
    }
    
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        image2=przeskalujObraz(image,getWidth(),getHeight());
        g.drawImage(image2,0,0,this);  
    } 
      
}