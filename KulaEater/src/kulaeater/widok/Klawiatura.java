package kulaeater.widok;

import static kulaeater.stale.Kierunek.*;


import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import kulaeater.widok.zdarzenia.*;


/**Klasa odpowiedzialna za obs�ug� zdarze� generowanych z klawiatury. 
 * @author K-E team
 *
 */
public class Klawiatura extends KeyAdapter {
	/**Referencja na widok do kt�rego nale�y.
	 * 
	 */
	private final Widok mojWidok;
	
	/**Konstruktor klasy Klawiatura.
	 * @param widok
	 */
	Klawiatura(Widok widok){
		mojWidok = widok;
	}
	

    public void keyPressed(KeyEvent key) {
        switch(key.getKeyCode()){
        case KeyEvent.VK_ESCAPE :
        	 mojWidok.kolejka.add(new DoMenuEvent());
        	 break;
    	case KeyEvent.VK_RIGHT :
    		 mojWidok.kolejka.add(new ZmianaSkretuEvent(PRAWO));
    		 break;
    	case KeyEvent.VK_LEFT :
    		 mojWidok.kolejka.add(new ZmianaSkretuEvent(LEWO));
    		 break;
    	case KeyEvent.VK_DOWN :
    		 mojWidok.kolejka.add(new ZmianaSkretuEvent(DOL));
    		 break;
    	case KeyEvent.VK_UP :
    		 mojWidok.kolejka.add(new ZmianaSkretuEvent(GORA));
    		 break;	
    	case KeyEvent.VK_P :
   		 	mojWidok.kolejka.add(new PauzaEvent());
   		 	break;	
    	case KeyEvent.VK_F11 :
    		//mojWidok.kolejka.add(new CheatKolejnyPoziomEvent());
    		break;    		
        }        
	}
}
