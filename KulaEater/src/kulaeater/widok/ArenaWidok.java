package kulaeater.widok;

import java.awt.BorderLayout;
import java.util.LinkedList;
import javax.swing.JPanel;
import kulaeater.stale.Kierunek;
import kulaeater.stale.RodzajPola;

/**Klasa odpowiadaj�ca za prezentacje pola gry.
 * @author K-E team
 *
 */
public class ArenaWidok extends JPanel {
	
	private static final long serialVersionUID = 1L;
	
	/**Pole reprezentuj�ce obiekt klasy KontrolkiWidok.
	 * 
	 */
	private final KontrolkiWidok kontrolki; 
	/**Pole reprezentuj�ce obiekt klasy PlanszaWidok.
	 * 
	 */
	private final PlanszaWidok plansza;
	/**Referencja na widok do kt�rego nale�y.
	 * 
	 */
	private final Widok mojWidok;
	
	
	/**Konstruktor klasy ArenaWidok.
	 * @param widok Widok do ktorego ma nalezec klasa
	 */
	ArenaWidok (Widok widok){
		mojWidok=widok;
		kontrolki = new KontrolkiWidok(mojWidok);
		plansza = new PlanszaWidok(mojWidok);
		setLayout(new BorderLayout());
		
		add(plansza,BorderLayout.CENTER);
		add(kontrolki,BorderLayout.SOUTH);
	}
	
	/**Metoda od�wie�aj�ca panel stanu gry. 
	 * @param zycia Ilo�� �y� KulaEatera.
	 * @param poziom Numer poziomu.
	 * @param bonus Aktualnie zebrany bonus.
	 * @param punkty Aktualny wynik gracza.
	 * @param czas Czas przeznaczony na doko�czenie poziomu.
	 */
	void odswiezKontrolki(int zycia, String poziom, String bonus, int punkty, int czas){
		kontrolki.odswiez(zycia, poziom, bonus, punkty, czas);
	}
	/**Metoda od�wie�aj�ca panel pola gry.
	 * @param uklad Uk�ad poziomu.
	 * @param pozPacman Pozycja KulaEatera.
	 * @param zwrot Zwrot KulaEatera.
	 * @param pozWrog Pozycja wrog�w.
	 * @param zaslona Parametr odpowiedzialny za ustawienie zas�ony ograniczaj�cej widoczno��.
	 */
	void odwiezPlansze(RodzajPola[][] uklad, double[] pozPacman, Kierunek zwrot, LinkedList<double[]> pozWrog, boolean zaslona){
		plansza.odswiez(uklad,pozPacman,zwrot,pozWrog,zaslona);
	}
	
}
