package kulaeater.widok;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Map;

/**Klasa odpowiadająca za prezentacje najlepszych wyników.
 * @author K-E team
 *
 */
public class ToplistaWidok extends JPanel{
	/**Pole zawierające informacje o topliscie.
	 * 
	 */
	private final JLabel lista;
	/**Referencja na Widok do którego należy.
	 * 
	 */
	private final Widok mojWidok;
	
	
	/**Konstruktor klasy ToplistaWidok.
	 * @param widok Referencja na Widok do którego należy tworzony obiekt.
	 */
	public ToplistaWidok(Widok widok) {
		mojWidok = widok;
		lista = new JLabel();
		setBackground(Color.BLACK);
        add(lista, BorderLayout.CENTER);
	}


	/**Metoda aktualizująca wyświetlaną topliste.
	 * @param topMapa Mapa zawierająca informacje o najlepszych wynikach/
	 */
	void aktualizujListe(Map<Integer,String> topMapa){
		String wynikiHtml = new String("");
		for(int i=1; i<=topMapa.size();i++){
			wynikiHtml =wynikiHtml + i +". "+ topMapa.get(i)+"<br>";
		}
		lista.setText("<html><center><font size=5 color=white><br><Br><b>TOPLISTA:</b><br><br>"+wynikiHtml+"<br></font></center></html>");
	}
}

