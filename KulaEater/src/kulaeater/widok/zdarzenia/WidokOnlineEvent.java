package kulaeater.widok.zdarzenia;


/**Zdarzenie przełączenia trybu gry.
 * @author K-E team
 *
 */
public class WidokOnlineEvent extends Event{
	boolean tryb;
	public WidokOnlineEvent( boolean tryb){
		this.tryb=tryb;
	}
	public boolean dajTryb(){
		return tryb;
	}
}
