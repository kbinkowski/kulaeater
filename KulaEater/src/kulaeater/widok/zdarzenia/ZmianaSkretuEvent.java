package kulaeater.widok.zdarzenia;

import kulaeater.stale.Kierunek;

/**Zdarzenie zmiany skr�tu KulaEatera.
 * @author K-E team
 *
 */
public class ZmianaSkretuEvent extends Event {
	private final Kierunek strzalka;

	public Kierunek dajKierunek() {
		return strzalka;
	}

	public ZmianaSkretuEvent(final Kierunek strzalka) {
		this.strzalka = strzalka;
	}
}
