package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class SmiercBonus extends Bonus {

	public SmiercBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.tracZyciePrzezBonus(true);
	}

	@Override
	protected void wylaczImpl() {
		mojModel.tracZyciePrzezBonus(false);
	}

}
