package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class SzybcyWrogowieBonus extends Bonus {

	public SzybcyWrogowieBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.przyspieszWrogow(true);

	}

	@Override
	protected void wylaczImpl() {
		mojModel.przyspieszWrogow(false);

	}

}
