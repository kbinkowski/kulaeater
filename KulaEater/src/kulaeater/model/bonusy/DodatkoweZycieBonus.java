package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class DodatkoweZycieBonus extends Bonus {

	public DodatkoweZycieBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.dodajDodatkoweZycie(true);
	}

	@Override
	protected void wylaczImpl() {
		mojModel.dodajDodatkoweZycie(false);

	}

}
