package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class ZamrozenieBonus extends Bonus {

	public ZamrozenieBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.zamrozPacmana(true);

	}

	@Override
	protected void wylaczImpl() {
		mojModel.zamrozPacmana(false);

	}

}
