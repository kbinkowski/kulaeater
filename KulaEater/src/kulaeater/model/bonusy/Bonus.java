package kulaeater.model.bonusy;

import kulaeater.model.Model;


public abstract class Bonus implements Runnable {
	protected final int czasTrwania;
	protected final Model mojModel;
	protected boolean wlaczony;
	protected abstract void wlaczImpl(); 
	protected abstract void wylaczImpl(); // NIE WOLAC - uzywac wylacz, ktre sprawdza wlaczenie
	public Bonus(Model model, int czas) {
		czasTrwania=czas*1000; //przeliczenie z sekund na milisekundy
		mojModel=model;
		Thread t = new Thread(this);
		t.start();
	}
	public void run() {
		wlacz();
		try {
			Thread.sleep(czasTrwania);
		} catch (InterruptedException e) {	
		}
		wylacz();

	}
	public void wylacz() {
		if (wlaczony) wylaczImpl();
		wlaczony=false;
	}
	public void wlacz(){
		wlaczony=true;
		wlaczImpl();
	}
}
