package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class WolniWrogowieBonus extends Bonus {

	public WolniWrogowieBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.spowolnijWrogow(true);

	}

	@Override
	protected void wylaczImpl() {
		mojModel.spowolnijWrogow(false);

	}

}
