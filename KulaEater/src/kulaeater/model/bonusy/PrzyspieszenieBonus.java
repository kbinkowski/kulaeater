package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class PrzyspieszenieBonus extends Bonus {

	public PrzyspieszenieBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.przyspieszPacmana(true);

	}

	@Override
	protected void wylaczImpl() {
		mojModel.przyspieszPacmana(false);

	}

}
