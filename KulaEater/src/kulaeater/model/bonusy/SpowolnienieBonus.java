package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class SpowolnienieBonus extends Bonus {

	public SpowolnienieBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.spowolnijPacmana(true);

	}

	@Override
	protected void wylaczImpl() {
		mojModel.spowolnijPacmana(false);

	}

}
