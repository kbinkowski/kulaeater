package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class ZjadanieWrogowBonus extends Bonus {

	public ZjadanieWrogowBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.zjadajWrogow(true);
	}

	@Override
	protected void wylaczImpl() {
		mojModel.zjadajWrogow(false);

	}

}
