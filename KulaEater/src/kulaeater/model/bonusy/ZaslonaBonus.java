package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class ZaslonaBonus extends Bonus {

	public ZaslonaBonus(Model model, int czas) {
		super(model,czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.wlaczZaslone(true);

	}

	@Override
	public void wylaczImpl() {
		mojModel.wlaczZaslone(false);
	}

}
