package kulaeater.model.bonusy;

import kulaeater.model.Model;

public class ZabicieWrogowBonus extends Bonus {

	public ZabicieWrogowBonus(Model model, int czas) {
		super(model, czas);
	}

	@Override
	protected void wlaczImpl() {
		mojModel.zabijWszystkichWrogow(true);
	}

	@Override
	protected void wylaczImpl() {
		mojModel.zabijWszystkichWrogow(true);

	}

}
