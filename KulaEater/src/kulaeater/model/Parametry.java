package kulaeater.model;

/** Klasa przechowuje parametry sta�e (w trakcie danej rozgrywki) parametry rozgrywki.
 * @author K-E Team
 *
 */
public class Parametry {
	/**Pr�dko�� KulaEeatra(Pacmana) w polach na miliskend�
	 * 
	 */
	private final double predkoscKe;
	/**Pr�dkos� wrog�w w polach na milisekund�
	 * 
	 */
	private double predkoscWrog;
	/**Ilo�� �y� przyznawana na starcie.
	 * 
	 */
	private final int iloscZyc;
	
	/**Ilo�� punkt�w przyznawana za zjedzeni kulki.
	 * 
	 */
	private final int pktKulka;
	
	/**Ilo�� punkt�w za pozosta�e do ko�ca gry �ycie.
	 * 
	 */
	private final int pktZycie;
	/**Ilo�� puntkow za sekund� pozosta�ego czasu na dany poziom.
	 * 
	 */
	private final int pktCzas;
	/**Ilo�� punkt�w za zdjedzenie / zabicie wroga
	 * 
	 */
	private final int pktWrog;
	/** Konstruktor klasy ustawiaj�cy odpowiednie pola
	 * @param predkoscKe
	 * @param predkoscWrog
	 * @param iloscZyc
	 * @param pktKulka
	 * @param pktWrog
	 * @param pktZycie
	 * @param pktCzas
	 */
	Parametry(double predkoscKe,double predkoscWrog, int iloscZyc,int pktKulka, int pktWrog, int pktZycie, int pktCzas){
		this.predkoscKe=predkoscKe;
		this.predkoscWrog=predkoscWrog;
		this.iloscZyc=iloscZyc;
		this.pktKulka=pktKulka;
		this.pktZycie=pktZycie;
		this.pktWrog=pktWrog;
		this.pktCzas=pktCzas;
	}
	
	/**
	 * @return pr�dko�� pacmana(kulaeatera)
	 */
	public double dajPredkoscKe(){
		return predkoscKe;
	}
	/**
	 * @return pr�dko�� wrog�w
	 */
	public double dajPredkoscWrog(){
		return predkoscWrog;
	}
	/**
	 * @return standardowa ilo�� �y�
	 */
	public int dajIloscZyc(){
		return iloscZyc;
	}
	public int dajPktKulka() {
		return pktKulka;
	}
	public int dajPktZycie() {
		return pktZycie;
	}
	public int dajPktCzas() {
		return pktCzas;
	}
	public int dajPktWrog() {
		return pktWrog;
	}

	/**Ustawia pr�dko�� wrog�w. Wo�ane przy zjedzeniu odpowiedniego bonusu.
	 * @param v pr�dk�� w polach na milisekund�
	 */
	public void ustawPredkoscWrog(double v) {
		predkoscWrog=v;
	}

}
