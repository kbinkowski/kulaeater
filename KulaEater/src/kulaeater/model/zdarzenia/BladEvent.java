package kulaeater.model.zdarzenia;

import kulaeater.widok.zdarzenia.Event;

public class BladEvent extends Event {
	private String komunikat;
	
	public BladEvent(String tekst){
		this.komunikat=tekst;
	}
	
	public String dajTekst(){
		return komunikat;
	}
	
}
