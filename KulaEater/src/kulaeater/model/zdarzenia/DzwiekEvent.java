package kulaeater.model.zdarzenia;

import kulaeater.widok.zdarzenia.Event;

public class DzwiekEvent extends Event {
	
	String rodzaj;
	
	public DzwiekEvent(String rodzaj){
		this.rodzaj=rodzaj;
	}
	
	public String dajRodzaj(){
		return rodzaj;
	}
}
