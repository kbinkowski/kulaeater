package kulaeater.model.zdarzenia;
import kulaeater.model.*;
import kulaeater.widok.zdarzenia.Event;
import java.util.LinkedList;
import static kulaeater.stale.Stale.*;
import kulaeater.stale.Kierunek;
import kulaeater.stale.RodzajPola;

public class NowyStanArenyEvent extends Event {
	private final Kontrolki tmpKontrolki;
	private final RodzajPola[][] tmpPlansza;
	private final double[] tmpPozPacman;
	private final Kierunek zwrotPacman;
	private LinkedList<double[]> tmpPozWrog;
	private final boolean tmpZaslona;
	
	public NowyStanArenyEvent(final Plansza plansza, final Pacman pacman, final LinkedList<Wrog> wrogowie, final Kontrolki kontrolki){
		tmpKontrolki=kontrolki;
		tmpPlansza=plansza.dajUklad();
		tmpPozPacman = new double[2];
		tmpPozPacman[0] = pacman.dajPozycjeX();
		tmpPozPacman[1] = pacman.dajPozycjeY();
		zwrotPacman = pacman.dajZwrot();
		tmpPozWrog = new LinkedList<double[]>();
		for (Wrog w : wrogowie){
			double[] poz = {w.dajPozycjeX(),w.dajPozycjeY()};
			tmpPozWrog.add(poz);
		}
		tmpZaslona=kontrolki.dajBonus().equals(BONUS_NAZWA_ZASLONA);
	}

	public Kontrolki dajKontrolki() {
		return tmpKontrolki;
	}

	public RodzajPola[][] dajUkladPlanszy() {
		return tmpPlansza;
	}

	public double[] dajPozPacman() {
		return tmpPozPacman;
	}

	public LinkedList<double[]> dajPozWrog() {
		return tmpPozWrog;
	}

	public boolean dajZaslona() {
		return tmpZaslona;
	}
	
	public Kierunek dajZwrotPacman() {
		return zwrotPacman;
	}
	
	
	
}
