package kulaeater.model;

import static kulaeater.stale.Kierunek.*;

/**Klasa reprezentuje Posta� pacmana. Jest istot� kt�ra dodatkowo potrafi je�� kulki.
 * @author K-E team
 *
 */
public class Pacman extends Istota {
	/**Ilo�� kulek ktore zjad� pacman.
	 * 
	 */
	private int zjedzoneKulki;
	Pacman(Model model, double pozX, double pozY,double predkosc) {
		super(model, pozX, pozY, predkosc);
		zjedzoneKulki=0;
	}
	
	int dajZjedzoneKulki() {
		return zjedzoneKulki;
	}

	/**Zerowa� przy przechodzeniu do kolejnego poziomu / �mierci
	 * 
	 */
	void zerujZjedzoneKulki(){
		zjedzoneKulki=0;
	}
	void umrzyj(){
		zerujZjedzoneKulki();
		ustawZwrot(ZERO);
		ustawSkret(ZERO);
		mojModel.smiercKulaEatera();
	}
	void zjedzKulke(){
		++zjedzoneKulki;
		mojModel.ZjedzonaKula();
	}

}
