package kulaeater.model;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;


import kulaeater.model.zdarzenia.BladEvent;
import kulaeater.model.zdarzenia.KoniecSerwerEvent;
import kulaeater.model.zdarzenia.ModelOnlineEvent;
import kulaeater.stale.RodzajPola;
import kulaeater.stale.ProtokolSieciowy;
import static kulaeater.stale.RodzajPola.*;
import static kulaeater.stale.Stale.*;


/**Klasa reprezentuj�ca klienta sieciowego (�r�d�o danych na temat gry). Posiada metody dostarczaj�ce informacje przesy�ane z serwera.
 * @author K-E team
 *
 */
public class KlientSieciowy implements ZrodloDanych, Runnable {
	
    /**Gniazdo klienta.
     * 
     */
    private Socket socket = null;
    /**Strumie� wej�ciowy.
     * 
     */
    private BufferedReader input;
    /**Strumie� wyj�ciowy.
     * 
     */
    private PrintWriter output;
    

    /**Pole reprezentuj�ce obiekt klasy Model.
     * 
     */
    private final Model model;

    /**Pole reprezentuj�ce obiekt klasy Toplista. Zawiera aktualn� informacje przes�an� z serwera.
     * 
     */
    private Toplista toplista;
    /**Pole reprezentuj�ce obiekt klasy Parametry. Zawiera aktualn� informacje przes�an� z serwera.
     * 
     */
    private Parametry parametry;
    /**Pole reprezentuj�ce obiekt klasy Plansza. Zawiera aktualn� informacje przes�an� z serwera.
     * 
     */
    private Plansza plansza;
    /**Pole reprezentuj�ce odpowiedz serwera na zapytanie czy wynik kwalifikuje si� na topliste.
     * 
     */
    private int decyzjaWynik;
    
    /**Pole zawieraj�ce informacje czy odpowiednie dane przechowywane w KliencieSieciowym s� aktualne;
     * 
     */
    private boolean aktualnaToplista=false;
    /**Pole zawieraj�ce informacje czy odpowiednie dane przechowywane w KliencieSieciowym s� aktualne;
     * 
     */
    private boolean aktualnaPlansza=false;
    /**Pole zawieraj�ce informacje czy odpowiednie dane przechowywane w KliencieSieciowym s� aktualne;
     * 
     */
    private boolean aktualneParametry=false;
    /**Pole zawieraj�ce informacje czy nast�pi�o wylogowanie;
     * 
     */
    private boolean wylogowalem=false;
    /**Pole zawieraj�ce informacje czy nast�pi�o zalogowanie;
     * 
     */
    private boolean zalogowalem=false;
    /**Pole zawieraj�ce informacje czy odpowiednie dane przechowywane w KliencieSieciowym s� aktualne;
     * 
     */
    private boolean aktualnaDecyzjaWynik=false;

    /**Pole wskazuj�ce na stan pracy Klienta. Gdy false - w�tek klienta ko�czy dzia�anie.
     * 
     */
    private boolean pracuj = false;
    
    
    /**Konstruktor klasy KlientSieciowy.
     * @param model Model.
     * @param host Adres serwera.
     * @param port Port serwera.
     * @throws Exception
     */
    public KlientSieciowy(Model model, String host, String port) throws Exception {
    		this.model=model;
        try {
            socket = new Socket(host, Integer.parseInt(port));
        } 
        catch (UnknownHostException e) {
            throw new Exception("Nieznany host."); 
        } 
        catch (IOException e) {
            throw new Exception("IO exception podczas pod��czania do serwera.");
        } 
        catch (NumberFormatException e) {
            throw new Exception("Port musi by� liczb� ca�kowit�.");
        }
        
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream(), true);
        } 
        catch (IOException ex) {
            throw new Exception("Problem w pobraniu strumieni.");
        }
        
        pracuj = true;
        new Thread(this).start();
        zaloguj();
    }

 

	public void run() {
        while (pracuj){
        	try{
	            String polecenie = input.readLine();
	            if (!wykonajPolecenie(polecenie)) {
	            	pracuj=false;
	            	wylogowalem=true;
	                output.close();
					input.close();
					socket.close();
	                break;
	            } 
        	}catch(IOException e){
        		model.kolejka.add(new BladEvent(e.toString()));	
        	}
        }    
        output = null;
        input = null;
        synchronized (this) {
            socket = null;
        }
    }

    
    /**Metoda odpowiedzialna za obs�ug� komunikat�w nadsy�anych z serwera. Zwraca false gdy przerywane po��czenie.
     * @param command Polecenie przes�ane z serwera.
     * @return Decyzja o po��czeniu.
     */
    private boolean wykonajPolecenie(String command) {
    	if(command!=null){
	    	StringTokenizer st = new StringTokenizer(command);
	        String cd = st.nextToken();
	        if (cd.equals(ProtokolSieciowy.KE_ZALOGOWALEM)) {
	        	zalogowalem=true;   
	        } 
	        else if (cd.equals(ProtokolSieciowy.PARAMETRY_TO)) {
	        	Double vKe=Double.parseDouble(st.nextToken());
	        	Double vWrog=Double.parseDouble(st.nextToken());
	        	int iloscZyc=Integer.parseInt(st.nextToken());
	        	int pktKulka=Integer.parseInt(st.nextToken()); 
	        	int pktWrog=Integer.parseInt(st.nextToken()); 
	        	int pktZycie=Integer.parseInt(st.nextToken());
	        	int pktCzas=Integer.parseInt(st.nextToken());
	        	parametry = new Parametry(vKe, vWrog, iloscZyc, pktKulka, pktWrog, pktZycie, pktCzas);
	        	aktualneParametry=true;
	        } 
	        else if (cd.equals(ProtokolSieciowy.TOP_LISTA_TO)) {	
	        	Map<Integer,String> topMapa=new HashMap<Integer,String>();
	        	int pozycja;
	        	String tekst;
	            for(int i =0; i<10; i++){
	            	pozycja = Integer.parseInt(st.nextToken());
	            	tekst = st.nextToken() + " " + st.nextToken();
	            	topMapa.put(pozycja, tekst);
	            }  
	            toplista = new Toplista(topMapa);
	            aktualnaToplista=true;
		    } 
	        else if (cd.equals(ProtokolSieciowy.TOP_AKCEPTUJE)) {
		    	decyzjaWynik = Integer.parseInt(st.nextToken());
		    	aktualnaDecyzjaWynik=true;
			} 
	        else if (cd.equals(ProtokolSieciowy.MAPA_DAJE_POZIOM)) {
				int numer=Integer.parseInt(st.nextToken());
				int czas=Integer.parseInt(st.nextToken());
				int[] pozycjaKe= new int[2];
					pozycjaKe[0]=Integer.parseInt(st.nextToken());
					pozycjaKe[1]=Integer.parseInt(st.nextToken());
				int iloscWrog=Integer.parseInt(st.nextToken());
				int[] pozycjaWrog= new int[2];
					pozycjaWrog[0]=Integer.parseInt(st.nextToken());
					pozycjaWrog[1]=Integer.parseInt(st.nextToken());
				RodzajPola[][] uklad=new RodzajPola[KAFLE_NA_WYS][KAFLE_NA_SZER];
				String pole;
				int iloscKulek=0;
				for (int k=0; k<KAFLE_NA_WYS; k++){
					for (int j=0; j<KAFLE_NA_SZER; j++){
						
						pole= st.nextToken();
						switch(Integer.parseInt(pole)){
						case 0:
							uklad[k][j]=MUR;
							break;
						case 247:
							uklad[k][j]=KULKOWE;
							++iloscKulek;
							break;
						case 63:
							uklad[k][j]=B_ZASLONA;
							++iloscKulek;
							break;
						case 71:
							uklad[k][j]=B_SMIERC;
							++iloscKulek;
							break;
						case 79:
							uklad[k][j]=B_ZAMROZ;
							++iloscKulek;
							break;
						case 87:
							uklad[k][j]=B_WOLNYPACMAN;
							++iloscKulek;
							break;
						case 95:
							uklad[k][j]=B_SZYBKIWROG;
							++iloscKulek;
							break;
						case 127:
							uklad[k][j]=B_PRZYSPIESZ;
							++iloscKulek;
							break;
						case 135:
							uklad[k][j]=B_JEDZWROGOW;
							++iloscKulek;
							break;
						case 143:
							uklad[k][j]=B_WOLNYWROG;
							++iloscKulek;
							break;
						case 151:
							uklad[k][j]=B_ZABIJWROGOW;
							++iloscKulek;
							break;
						case 159:
							uklad[k][j]=B_ZYCIE;
							++iloscKulek;
							break;
						default:
							uklad[k][j]=MUR;
						}
					}
				}		
				plansza = new Plansza(numer,uklad,czas,pozycjaKe,pozycjaWrog,iloscWrog,iloscKulek);
				aktualnaPlansza=true;
			} 
	        else if (cd.equals(ProtokolSieciowy.KE_WYLOGOWALEM)) {
				return false;
			} 
	        else if (cd.equals(ProtokolSieciowy.KE_KONCZE)) {
	        	model.kolejka.add(new KoniecSerwerEvent());
	        	model.kolejka.add(new ModelOnlineEvent());
			} 
	        else if (cd.equals(ProtokolSieciowy.NULLCOMMAND)) {
	        } 
	        return true;
	    }
    	return false;
    }
    
  
     /**Metoda loguj�ca klienta do serwera. Wysy�a ��danie.
     * 
     */
    private void zaloguj(){
    	if (socket != null)
    		wyslij(ProtokolSieciowy.KE_ZALOGUJ);
    	while(!zalogowalem){
    		try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				model.kolejka.add(new BladEvent(e.toString()));	
			}
    	}
    	zalogowalem=false;
    }
    /**Metoda wylogowywuj�ca klienta do serwera. Wysy�a ��danie.
     * 
     */
    private void wyloguj() {
        if (socket != null)
            wyslij(ProtokolSieciowy.KE_WYLOGUJ);
        	while(!wylogowalem){
        		try {
    				Thread.sleep(5);
    			} catch (InterruptedException e) {
    				model.kolejka.add(new BladEvent(e.toString()));	
    			}
        	}
        wylogowalem=false;
    }
    
    
    /**Metoda wysy�aj�ca do serwera komunikat.
     * @param polecenie Komunikat przesy�any do serwera.
     */
    private void wyslij(String polecenie) {
        if (output != null) {
        	output.println(polecenie);
        }	            
    }

    @Override
    public Parametry dajParametry(){
    	wyslij(ProtokolSieciowy.PARAMETRY_JAKIE);
    	while(!aktualneParametry){	
    		try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				model.kolejka.add(new BladEvent(e.toString()));	
			}	
    	}
    	aktualneParametry=false;
    	return parametry;
    }
   
    @Override
    public Plansza dajPlansza(int numer){
    	wyslij(ProtokolSieciowy.MAPA_DAJ_POZIOM + " " + numer);
    	while(!aktualnaPlansza){	
    		try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				model.kolejka.add(new BladEvent(e.toString()));	
			}
    	}
    	aktualnaPlansza=false;
    	return plansza;
    }
    
    @Override
    public Toplista dajToplista(){
    	wyslij(ProtokolSieciowy.TOP_LISTA_JAKA);
    	while(!aktualnaToplista){
    		try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				model.kolejka.add(new BladEvent(e.toString()));	
			}
    	}
    	aktualnaToplista=false;
    	return toplista;
    }
    
    @Override
    public boolean czyDobryWynik(int wynik){
    	wyslij(ProtokolSieciowy.TOP_ZGLASZAM_WYNIK+" "+wynik);
    	while(!aktualnaDecyzjaWynik){
    		try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				model.kolejka.add(new BladEvent(e.toString()));	
			}
    	}
    	aktualnaDecyzjaWynik=false;
    	if (decyzjaWynik>0) return true;
    	else return false;
    }
    
    @Override
    public void zglaszamWynik(int wynik, String imie){
    	wyslij(ProtokolSieciowy.TOP_DANE_ZAAKCEPTOWANEGO+" "+wynik+" "+imie);
    }


	@Override
	public void zakoncz() {
		wyloguj();	
	}
}