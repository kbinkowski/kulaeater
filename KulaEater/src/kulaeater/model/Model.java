package kulaeater.model;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;


import kulaeater.stale.Kierunek;
import kulaeater.stale.RodzajPola;
import kulaeater.widok.zdarzenia.DoMenuEvent;
import kulaeater.widok.zdarzenia.Event;
import kulaeater.model.bonusy.*;
import kulaeater.model.zdarzenia.*;
import static kulaeater.stale.RodzajPola.*;

import static kulaeater.stale.Stale.*;

/**
 * Klasa reprezentuje Model gry. Przechowuje dane okreslajace stan rozgrywki (ale tez menu itp) oraz dostarcza
 * interfejsu pozwalaj�cego na zmian� i odczytanie tego stanu. Klasa implementruje interfejs Runnable -
 * aktualizuje w petli swoj stan.
 * @author K-E Team
 *
 */
public class Model implements Runnable{

	/**Reprezentuje zrodlo danych z ktorych czerpie informacje potrzebne do gry. 
	 *
	 */
	private ZrodloDanych dane;
	/**Reprezentuje postac Pacmana(KulaEatera) obecna w danym modelu.
	 * 
	 */
	private Pacman pacman;
	/**Reprezentuje wrogow (duchy) obecne w danym modelu.
	 * 
	 */
	private LinkedList<Wrog> wrogowie;
	/**Reprezentuje parametry rozgrywki. Wczytywane ze ZrodlaDanych.
	 * 
	 */
	private Parametry parametry;
	/**Reprezentuje obecny stan punktow, zyc, czasu oraz obecnego poziomu, bonusu.
	 * 
	 */
	private Kontrolki kontrolki;
	/**Reprezentuje obecny bonus.
	 * 
	 */
	private Bonus bonus;
	
	/**Pole przechowuje przy zapauzowaniu modelu czas pozostaly do konca danej planszy. W celu wznowienia gry z poprzednim czasem.
	 * 
	 */
	private int czasPauzy;
	
	 
	/**Flagi stanu rozgrywki. Pauza-dale mozliwosc wznowienia. Rozgrywka-koniecznosc rozpoczecia od poczatku.
	 * 
	 */
	private boolean pauza=true , rozgrywka=false;
	
	
	/**Referencja na kolejke zdarzen aby model mogl generowac zdarzenia wymagaj�ce obs�ugi przez sterownik.
	 * 
	 */
	final BlockingQueue<Event> kolejka;
	
	/**Reprezentuje obecna plansze gry. Zmieniana przy przechodzeniu miedzy poziomami.
	 * 
	 */
	Plansza obecnaPlansza; // pakietowa bo silnie uzalezniona od pozostalych klas pakietu
	
	
	/**Konstruktor Modelu ustawia referencje na kolejk� zdarze� oraz tworzy CzytnikPlikow przypisujac go do modelu.
	 * @param kolejka 
	 */
	public Model(BlockingQueue<Event> kolejka){
		this.kolejka = kolejka;
		dane=new CzytnikPlikow(this);
	}
	

	/**Zwraca napis - informacje o autorach gry.
	 * @return
	 */
	public String dajAutorow(){
		return "<html><center><font size=30 color=white><br><Br><b>AUTORZY:</b><br>Damian Gadomski<br>Kamil Bi�kowski</font></center></html>";
	}
	
	/**Zakancza prace modelu. Nalezy wolac przy zakonczeniu jego uzywania. Zamyka ZrodloDanych.
	 * 
	 */
	public void zakoncz(){
		dane.zakoncz();
	}
	
	/**Metoda pozwalajaca przejsc do nastepnego poziomu bez spelnienia warunkow rozgrywki.
	 * 
	 */
	public void cheatKolejnyPoziom(){
		ustawPoziom(obecnaPlansza.dajNumer()+1);
	}
	
	/**Ustawia obeny poziom na konkretny - pobierajac go ze �r�d�a danych.
	 * @param nr
	 */
	private void ustawPoziom(int nr){
		ustawPlansze(dane.dajPlansza(nr));
	}
	/**Wczytujke parametry rozgrywki ze �r�d�a danych.
	 * 
	 */
	private void wczytajParametry(){
		parametry=dane.dajParametry();
		pacman = new Pacman(this,0,0, parametry.dajPredkoscKe());
		kontrolki= new Kontrolki(parametry.dajIloscZyc()-1,"","Brak poziomu.",0,0);
	}
	
	/**Ustawia plansze do gry. Aby nie zmieniac planszy parametru - zostaje on kopiowany.
	 * @param plansza plansza do ustawienia
	 */
	private void ustawPlansze(Plansza plansza){
		if(bonus!=null)bonus.wylacz();
		kontrolki.ustawCzas(plansza.dajCzas());
		kontrolki.ustawPoziom("Poziom:"+plansza.dajNumer());
		wrogowie= new LinkedList<Wrog>();
		for(int i=0;i<plansza.dajIloscWrog();++i){
			wrogowie.add(new Wrog(this, plansza.dajPozycjaWrog()[0], plansza.dajPozycjaWrog()[1], parametry.dajPredkoscWrog()));
		}
		pacman.ustawPozycje(plansza.dajPozycjaKe()[0],plansza.dajPozycjaKe()[1]);
		pacman.zerujZjedzoneKulki();
		obecnaPlansza=plansza.clone(); //kopiowanie
	}
	
	
	
	
	/**Metoda rozpoczynajaca rozgrywke. Wo�a� przy rozpoczeniu wyrazeniu ch�ci rozpocz�cia gry.
	 * 
	 */
	public void startujRozgrywke(){
		
		if(!rozgrywka){
			wczytajParametry();
			ustawPoziom(1);
			rozgrywka=true;
		}
		pauza=false;
		
	}
	
	/**Metoda ko�czy rozgrywk�. Wo�a� gdy u�ytkownik wyrazi ch�� zako�czenia LUB przejdzie ca�� gr�.
	 * @return liczba zdobytych punkt�w
	 */
	public int zakonczRozgrywke(){
		kontrolki.ustawPunkty(kontrolki.dajPunkty()+parametry.dajPktZycie()*kontrolki.dajZycia());
		rozgrywka=false;
		return kontrolki.dajPunkty();
		//kolejka.add(new KoniecRozgrywkiEvent(kontrolki.dajPunkty()));
	}
	
	/**Metoda oblicza stan wszytskich obiektow modelu na podstawie ich stanu obecnego i odstepu czasu.
	 * @param czasP Poprzedni czas aktualizacji. Slu�y do obliczenia odstepu czasu.
	 */
	private void aktualizujRozgrywke(long czasP){
		//czy sa jeszcze kulki	-> dalej NIE? kolejny poziom	
		if(pacman.dajZjedzoneKulki()==obecnaPlansza.dajIloscKulek()){
			kontrolki.ustawPunkty(kontrolki.dajPunkty()+parametry.dajPktCzas()*kontrolki.dajCzas());
			if(obecnaPlansza.dajNumer()>9){
				kolejka.add(new DoMenuEvent());
			}
			else{
				ustawPoziom(obecnaPlansza.dajNumer()+1);
			}
		}
		
		//czy czas nie skonczyl si� - TAK? zabij pacmana
		if (kontrolki.dajCzas()<1){
			tracZycie();
			//gra=false;
		}
		
		//czy nie na polu z duchem TAK? zabij pacmana LUB zjedz ducha
		for (Wrog w : wrogowie){
			if (Math.round(w.dajPozycjeX())==Math.round(pacman.dajPozycjeX()) & Math.round(w.dajPozycjeY())==Math.round(pacman.dajPozycjeY())){
				if(bonus instanceof ZjadanieWrogowBonus){
					zjedzWroga(w);
				}
				else{
					tracZycie();
				}
				
			}
		}
		
		//czy nie na polu z kulk� -/-> zjedz + punkty (bonus jest traktowany jako kulka)
		RodzajPola obecnePole=obecnaPlansza.jakiePole((int)Math.round(pacman.dajPozycjeY()), (int)Math.round(pacman.dajPozycjeX()));
		if(obecnePole==KULKOWE){
			zjedzKulke(Math.round(pacman.dajPozycjeX()), Math.round(pacman.dajPozycjeY()));
		}
		else if(obecnePole!=PUSTKA){
			zjedzKulke(Math.round(pacman.dajPozycjeX()), Math.round(pacman.dajPozycjeY()));
			aktywujBonus(obecnePole);
		}
		
		//przerysuj postacie ddbajac o mury
		kolejka.add(new NowyStanArenyEvent(obecnaPlansza, pacman, wrogowie, kontrolki));
		pacman.aktualizuj(czasP);
		for (Wrog w : wrogowie){
			w.gdzieIsc();
			w.sledzPacmana();
			w.aktualizuj(czasP);
		}
	}
	/**Jezeli bonus jest obecny wylacza go.
	 * 
	 */
	private void wylaczObecnyBonus(){
		if(bonus!=null){
			bonus.wylacz();
		}
	}
	/**Tworzy bonus adekwatny do typu odwiedzoneg pola.
	 * @param pole Typ odwiedzonego pola
	 */
	private void aktywujBonus(RodzajPola pole){
		switch(pole){
		case B_JEDZWROGOW :
			wylaczObecnyBonus();
			bonus=new ZjadanieWrogowBonus(this,CZAS_BONUSU_SEK);
			break;
		case B_ZASLONA :
			wylaczObecnyBonus();
			bonus=new ZaslonaBonus(this,CZAS_BONUSU_SEK);
			break;
		case B_PRZYSPIESZ :
			wylaczObecnyBonus();
			bonus=new PrzyspieszenieBonus(this, CZAS_BONUSU_SEK);
			break;
		case B_SMIERC :
			wylaczObecnyBonus();
			bonus=new SmiercBonus(this,CZAS_BONUSU_SEK);
			break;
		case B_SZYBKIWROG :
			wylaczObecnyBonus();
			bonus=new SzybcyWrogowieBonus(this,CZAS_BONUSU_SEK);
			break;
		case B_WOLNYPACMAN :
			wylaczObecnyBonus();
			bonus=new SpowolnienieBonus(this,CZAS_BONUSU_SEK);
			break;
		case B_WOLNYWROG :
			bonus=new WolniWrogowieBonus(this,CZAS_BONUSU_SEK);
			wylaczObecnyBonus();
			break;
		case B_ZABIJWROGOW :
			bonus=new ZabicieWrogowBonus(this,CZAS_BONUSU_SEK);
			wylaczObecnyBonus();
			break;
		case B_ZAMROZ :
			wylaczObecnyBonus();
			bonus=new ZamrozenieBonus(this,CZAS_BONUSU_SEK);
			break;
		case B_ZYCIE :
			wylaczObecnyBonus();
			bonus=new DodatkoweZycieBonus(this, CZAS_BONUSU_SEK);
			break;
		default :
			break;
		}
	}
	
	/**Zabija postac Pacmana i odpowiednio ustawia stan Modelu.
	 * 
	 */
	public void tracZycie() {
		if(kontrolki.dajZycia()>0){
			pacman.umrzyj();
			kontrolki.ustawZycia(kontrolki.dajZycia()-1);
			kontrolki.ustawCzas(obecnaPlansza.dajCzas());
			obecnaPlansza.wypelnijKulkami();
			pacman.ustawPozycje(obecnaPlansza.dajPozycjaKe()[0], obecnaPlansza.dajPozycjaKe()[1]);
			for(Wrog w : wrogowie){
				w.ustawPozycje(obecnaPlansza.dajPozycjaWrog()[0], obecnaPlansza.dajPozycjaWrog()[1]);
			}
			if(bonus!=null & !(bonus instanceof SmiercBonus)){
				bonus.wylacz();
			}
		}
		else{
			kolejka.add(new DoMenuEvent());
		}
	}
	/**Odpowiednio ustawia stan modelu po zjedzeniu kulki. 
	 * @param x Wspolrzednia X kulki.
	 * @param y	Wspolrzedna Y kulki.
	 */
	private void  zjedzKulke(double x, double y){
		pacman.zjedzKulke();
		obecnaPlansza.usunKulke((int)Math.round(x), (int)Math.round(y));
		kontrolki.ustawPunkty(kontrolki.dajPunkty()+parametry.dajPktKulka());
	}
	/**Zabija wroga i odpowiednio ustawia stan modelu.
	 * @param w Wrog do zabicia.
	 */
	private void zjedzWroga(Wrog w){
		w.ustawPozycje(obecnaPlansza.dajPozycjaWrog()[0], obecnaPlansza.dajPozycjaWrog()[1]);
		kontrolki.ustawPunkty(kontrolki.dajPunkty()+parametry.dajPktWrog());
	}
	/**Ustawia ch�� skr�tu pacmana.
	 * @param kierunek w okreslonym kierunku.
	 */
	public void skrecPacmana(Kierunek kierunek) {
		pacman.ustawSkret(kierunek);
	}

	/**
	 * @return informacja o pauzie true=w��czona
	 */
	public boolean czyWlaczonaPauza(){
		return pauza;
	}
	/**
	 * @return informacja o trwaniu rozgrywki true=trwa
	 */
	public boolean czyTrwaRozgrywka(){
		return rozgrywka;
	}
	/**Metoda s�uzy do wlaczania i wy�aczania pauzy. Dzia�a na zasacie prze��cznika.
	 * 
	 */
	public void wlaczPauze(){
		if(!pauza){
			pauza=true;
			czasPauzy = kontrolki.dajCzas();
		}
		else{
			kontrolki.ustawCzas(czasPauzy);
			pauza=false;
		}		
	}
	
	@Override
	public void run() {
		long czasP = System.currentTimeMillis();
		while(true){
			if (!pauza & rozgrywka) {
				aktualizujRozgrywke(czasP);
			}
				czasP = System.currentTimeMillis();	
			try {
				Thread.sleep(15);
			} catch (InterruptedException e) {
				kolejka.add(new BladEvent("B��d w�tku g��wnego gry."));
			}
		}
	}


	/**Ustawia bonus ograniczajacy widocznosc.
	 * @param tak
	 */
	public void wlaczZaslone(boolean tak) {
		kontrolki.ustawBonus(tak?BONUS_NAZWA_ZASLONA:"");
	}
	public void tracZyciePrzezBonus(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_SMIERC:"");
		if(tak)tracZycie();
	}
	public void spowolnijPacmana(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_ZWOLNIENIE:"");
		pacman.ustawPredkosc(tak?parametry.dajPredkoscKe()/2:parametry.dajPredkoscKe());
	}
	public void zamrozPacmana(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_ZAMROZENIE:"");
		pacman.ustawPredkosc(tak?0:parametry.dajPredkoscKe());
	}
	public void przyspieszWrogow(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_SZYBKIWROG:"");
		parametry.ustawPredkoscWrog(tak?parametry.dajPredkoscWrog()*2:parametry.dajPredkoscWrog()/2);
	}
	public void spowolnijWrogow(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_WOLNYWROG:"");
		parametry.ustawPredkoscWrog(tak?parametry.dajPredkoscWrog()/2:parametry.dajPredkoscWrog()*2);
	}
	public void przyspieszPacmana(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_PRZYSPIESZENIE:"");
		pacman.ustawPredkosc(tak?parametry.dajPredkoscKe()*2:parametry.dajPredkoscKe());
	}
	public void zjadajWrogow(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_ZJADANIEWROGOW:"");
		if(!tak)bonus=null;
		}
	public void zabijWszystkichWrogow(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_ZABIJWSZYSTKICH:"");
			if(tak){
			for(Wrog w : wrogowie){
				zjedzWroga(w);
			}
		}
	}
	public void dodajDodatkoweZycie(boolean tak){
		kontrolki.ustawBonus(tak?BONUS_NAZWA_ZYCIE:"");
		if(tak)kontrolki.ustawZycia(kontrolki.dajZycia()+1);
	}

	/**Bada czy wynik kwalifikuja si� na toplist� obecnego �r�d�a danych.
	 * @param wynik
	 * @return flaga true=kwalifikuje sie
	 */
	public boolean czyWynikDobryNaTopliste(int wynik){
		return dane.czyDobryWynik(wynik);
	}
	public void ustawZrodloDanych(ZrodloDanych zrodlo){
		dane.zakoncz();
		dane = zrodlo;
	}
	public Toplista dajToplista(){
		return dane.dajToplista();
	}
	public void dodajWpisToplisty(int wynik, String imie){
		dane.zglaszamWynik(wynik, imie);
	}
	public double dajPredkoscWrogow(){
		return parametry.dajPredkoscWrog();
	}
	/**
	 * @return wartosc o indeksie 0 to pozycja X a 1 to Y.
	 */
	public double[] dajPozycjePacmana(){
		double[] poz = {pacman.dajPozycjeX(),pacman.dajPozycjeY()};
		return poz;
	}
	
	public void smiercKulaEatera(){
		kolejka.add(new DzwiekEvent("smierc"));
	}
	
	public void ZjedzonaKula(){
		kolejka.add(new DzwiekEvent("zjedzenie"));
	}
}
