package kulaeater.model;

import static kulaeater.stale.Stale.*;
import static kulaeater.stale.RodzajPola.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kulaeater.model.zdarzenia.BladEvent;
import kulaeater.stale.RodzajPola;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;


/** Klasa reprezentuj�ca �r�d�o danych, wykonuj�ca operacje na lokalnych plikach konfiguracyjnych.
 * @author K-E team
 *
 */
public class CzytnikPlikow implements ZrodloDanych{
	
	 /**Pole reprezentuj�ce obiekt klasy Model.
     * 
     */
	private final Model mojModel;

	 /**Pole reprezentuj�ce obiekt klasy Toplista.
     * 
     */
	private Toplista toplista;
	/**Pole reprezentuj�ce obiekt klasy Parametry. 
     * 
     */
	private Parametry parametry;
	/**Tablica zawieraj�ca obiekty klasy Plansza.
     * 
     */
	private Plansza[] plansza;
	
	/**Konstruktor klasy CzytnikPlikow.
	 * @param model Model
	 */
	public CzytnikPlikow(Model model){
		this.mojModel=model;
		wczytajParametry();
		wczytajPoziomy();
		wczytajTopLista();
	}
	
	/**Metoda wczytuj�ca parametry gry z lokalnych plik�w konfiguracyjnych. Wo�ana z konstruktora klasy CzytnikPlik�w.
	 * 
	 */
	private void wczytajParametry(){
		try{
			Element korzen;
			File xmlPlik = new File(SCIEZKA_PARAMETRY_XML);
			SAXBuilder builder = new SAXBuilder();
			Document xml = (Document) builder.build(xmlPlik);
			korzen = xml.getRootElement();
			if (korzen.getName().equals("parametrygry")){
				Element el;
				// predkosci
				el =   korzen.getChild("predkosc");
				double predkoscKe =  Double.parseDouble(el.getAttributeValue("ke")); 
				double predkoscWrog =  Double.parseDouble(el.getAttributeValue("wrog")); 
				//ilosc zyc
				el =   korzen.getChild("zycie");
				int iloscZyc = Integer.parseInt(el.getAttributeValue("ilosc"));
				//punktacja
				el = korzen.getChild("punktacja");
				
				int pk = Integer.parseInt(el.getAttributeValue("kulka"));
				int pw = Integer.parseInt(el.getAttributeValue("wrog"));
				int pz = Integer.parseInt(el.getAttributeValue("zycie"));
				int pc = Integer.parseInt(el.getAttributeValue("czas"));
				parametry = new Parametry(predkoscKe, predkoscWrog, iloscZyc, pk,pw,pz,pc);
			}
		} catch (JDOMException e) {
			mojModel.kolejka.add(new BladEvent(e.toString()));	
		} catch (IOException e) {
			mojModel.kolejka.add(new BladEvent(e.toString()));	
		}
	}
	/**Metoda wczytuj�ca poziomy gry z lokalnych plik�w konfiguracyjnych. Wo�ana z konstruktora klasy CzytnikPlik�w.
	 * 
	 */
	private void wczytajPoziomy(){
		plansza = new Plansza[10];
		String tekst;
		for (int numer=1; numer<=10; numer++){
			try{
				BufferedReader strumien = new BufferedReader(new FileReader("./poziomy/"+numer+".pgm"));
				RodzajPola[][] uklad = new RodzajPola[KAFLE_NA_WYS][KAFLE_NA_SZER];
				strumien.readLine(); // format pliku
				tekst = strumien.readLine();
				int czas = Integer.parseInt(tekst.split(" ")[2]);
				tekst = strumien.readLine(); // pozycja kula eatera
				int[] pozycjaKe = {Integer.parseInt(tekst.split(" ")[2]),Integer.parseInt(tekst.split(" ")[3])};
				tekst = strumien.readLine(); // info o wrogach
				int iloscWrog = Integer.parseInt(tekst.split(" ")[2]);
				int[] pozycjaWrog = {Integer.parseInt(tekst.split(" ")[3]),Integer.parseInt(tekst.split(" ")[4])};
				strumien.readLine(); // rozmiar
				strumien.readLine(); // zakres 
				int iloscKulek=0;
				for (int k=0; k<KAFLE_NA_WYS; k++){
					for (int j=0; j<KAFLE_NA_SZER; j++){
						
						switch(Integer.parseInt(strumien.readLine())){
						case 0:
							uklad[k][j]=MUR;
							break;
						case 247:
							uklad[k][j]=KULKOWE;
							++iloscKulek;
							break;
						case 63:
							uklad[k][j]=B_ZASLONA;
							++iloscKulek;
							break;
						case 71:
							uklad[k][j]=B_SMIERC;
							++iloscKulek;
							break;
						case 79:
							uklad[k][j]=B_ZAMROZ;
							++iloscKulek;
							break;
						case 87:
							uklad[k][j]=B_WOLNYPACMAN;
							++iloscKulek;
							break;
						case 95:
							uklad[k][j]=B_SZYBKIWROG;
							++iloscKulek;
							break;
						case 127:
							uklad[k][j]=B_PRZYSPIESZ;
							++iloscKulek;
							break;
						case 135:
							uklad[k][j]=B_JEDZWROGOW;
							++iloscKulek;
							break;
						case 143:
							uklad[k][j]=B_WOLNYWROG;
							++iloscKulek;
							break;
						case 151:
							uklad[k][j]=B_ZABIJWROGOW;
							++iloscKulek;
							break;
						case 159:
							uklad[k][j]=B_ZYCIE;
							++iloscKulek;
							break;
						default:
							uklad[k][j]=MUR;
						}
					
					}
				}
				strumien.close();		
				plansza[numer-1] = new Plansza(numer,uklad,czas,pozycjaKe,pozycjaWrog,iloscWrog,iloscKulek); 
			} catch(IOException e){
				mojModel.kolejka.add(new BladEvent(e.toString()));	
			}
		}
	}
	/**Metoda wczytuj�ca topliste z lokalnych plik�w konfiguracyjnych. Wo�ana z konstruktora klasy CzytnikPlik�w.
	 * 
	 */
	private void wczytajTopLista(){
		try{
			Map<Integer,String> topMapa=new HashMap<Integer,String>();
			File xmlPlikTopka = new File(SCIEZKA_TOPLISTA_XML);
			SAXBuilder builder = new SAXBuilder();
			Document xml= (Document) builder.build(xmlPlikTopka);
			Element korzenTopki = xml.getRootElement();
			if (korzenTopki.getName().equals("toplista")){
				for (Element el : (List<Element>) korzenTopki.getChildren("gracz")){
					topMapa.put(Integer.parseInt(el.getAttributeValue("pozycja")), el.getText()+" "+el.getAttributeValue("punkty"));
				}
			}
			toplista = new Toplista(topMapa);
		} catch (JDOMException e) {
			mojModel.kolejka.add(new BladEvent(e.toString()));	
		} catch (IOException e) {
			mojModel.kolejka.add(new BladEvent(e.toString()));	
		}
	}
	
	/**Metoda zapisuj�ca aktualnie przechowywan� toplist� do pliku konfiguracyjnego.
	 * 
	 */
	public void zapiszToplista(){
		try {
			Map<Integer,String> topMapa = toplista.dajTopMapa();
			BufferedWriter strumien = new BufferedWriter(new FileWriter(SCIEZKA_TOPLISTA_XML));
			XMLOutputter builder = new XMLOutputter();
			Element root = new Element("toplista");
			Document xml = new Document(root);
			Element el;
			for (int i=0; i<10; i++){
				el= new Element("gracz");
				el.setAttribute("pozycja",""+(i+1));
				el.setAttribute("punkty",""+topMapa.get(i+1).split(" ")[1]);
				el.setText(topMapa.get(i+1).split(" ")[0]);
				root.addContent(el);
			}
			builder.output(xml, strumien);
		} catch (IOException e) {
			mojModel.kolejka.add(new BladEvent(e.toString()));	
		}
	}
	@Override
    public Parametry dajParametry(){
    	return parametry;
    }
    @Override
    public Plansza dajPlansza(int numer){
    	return plansza[numer-1];
    }
    @Override
    public Toplista dajToplista(){
    	return toplista;
    }
    /**Metoda sprawdzaj�ca czy dany podany wynik kwalifikuje si� na topliste.
	 * @param wynik Wynik gracza.
	 * @return Pozycja zakwalifikowanego gracza. Gdy niekwalifikuje si� 0.
	 */
    public boolean czyDobryWynik(int wynik){
    	if (Integer.parseInt(toplista.dajTopMapa().get(10).split(" ")[1])<wynik)
    		return true;
    	else 
    		return false;
    }
    
    /**Metoda dodaj�ca do toplisty gracza, jego imi� i wynik.
	 * @param wynik Wynik gracza.
	 * @param imie Imi� gracza.
	 */
    public void zglaszamWynik(int wynik, String imie){
    	Map<Integer, String> topMapa = toplista.dajTopMapa();
    	String pozycja;
    	int i;
		for (i=10; i>0; i--){
			if (Integer.parseInt(topMapa.get(i).split(" ")[1])>=wynik) break;
		}
		i=i+1;
		for (int j=10; j>i; j--){
			pozycja=topMapa.get(j-1);
			topMapa.put(j, pozycja);
		}
		topMapa.put(i, imie+" "+wynik);
		toplista.ustawTopMapa(topMapa);
    }

	@Override
	public void zakoncz() {
		zapiszToplista();
	}
	
}
