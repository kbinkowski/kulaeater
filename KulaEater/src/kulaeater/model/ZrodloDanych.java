package kulaeater.model;

/**Klasa reprezentuje abstrakcyjne �r�d�o danych z kt�rego mo�e korzysta� model gry.
 * @author KE team
 *
 */
public interface ZrodloDanych {
	/**
	 * @return toplista najlepszych graczy
	 */
	Toplista dajToplista();
	/**
	 * @param numer numer planszy kt�rej �adamy
	 * @return plansza o zadanym numerze
	 */
	Plansza dajPlansza(int numer);
	/**
	 * @return parametry gry
	 */
	Parametry dajParametry();
	/**Metoda wo�ana przy zaka�czaniu pracy ze �r�d�em danych. Pozwala mu na
	 * zamkni�cie plik�w / po��czenia internetowego itp. zale�nie od konkretnego �r�d�a.
	 * 
	 */
	void zakoncz();
	/**Metoda bada wynik punktowy kwalifikuje si� na toplist�.
	 * @param wynik liczba zdobytych punkt�w
	 * @return true = kiedy wynik dostatecznie dobry
	 */
	boolean czyDobryWynik(int wynik);
	/**Metoda dopisuje wynik do listy najlepszych wynik�w
	 * @param wynik liczba zdobytych punkt�w
	 * @param imie imi� gracza
	 */
	void zglaszamWynik(int wynik, String imie);
}
