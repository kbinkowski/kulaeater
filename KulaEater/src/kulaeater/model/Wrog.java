package kulaeater.model;

import static kulaeater.stale.Kierunek.*;

import java.util.Random;


/**Klasa reprezentuje wroga - rodzaj istoty zdolnej 
 * do �ledzenia Pacmana (KulaEateara) oraz losowego przemieszczania sie.
 * @author KE Team
 *
 */
public class Wrog extends Istota {
	Wrog(Model model, double pozX, double pozY, double predkosc){
		super(model,pozX,pozY,predkosc);
	}
	
	/**Metoda sprawdza pozcyj� Pacmana i wrazie gdy jest on widoczny dla instancji wroga
	 * zwi�ksze pr�dko�� wroga i ustawia kierunek w stron� pacmana. W przeciwnym wypadku
	 * nie zmienia kierunku i ustawia domy�ln� pr�dko��.
	 * 
	 */
	void sledzPacmana(){
			predkosc=mojModel.dajPredkoscWrogow();
			if(mojModel.obecnaPlansza.czyMurNaDrodze((int)Math.round(dajPozycjeX()),(int) Math.round(dajPozycjeY()),(int) Math.round(mojModel.dajPozycjePacmana()[0]), (int)Math.round(mojModel.dajPozycjePacmana()[1]))){
				if(Math.round(dajPozycjeX())==Math.round(mojModel.dajPozycjePacmana()[0])){
					predkosc=mojModel.dajPredkoscWrogow()*2;
					ustawSkret(dajPozycjeY()>mojModel.dajPozycjePacmana()[1]?GORA:DOL);
				}
				if(Math.round(dajPozycjeY())==Math.round(mojModel.dajPozycjePacmana()[1])){
					predkosc=mojModel.dajPredkoscWrogow()*2;
					ustawSkret(dajPozycjeX()>mojModel.dajPozycjePacmana()[0]?LEWO:PRAWO);
				}
			}
	}
	
	/**Metoda implemenuje algorym losowego przemieszczania si�.
	 * W zale�no�ci od losowych zmiennych ustawia skr�t wroga.
	 * 
	 */
	void gdzieIsc(){
		Random los = new Random(); 
		if(ileMogePrzedSiebie(2)>0){
			if(los.nextDouble()<0.01){
			
				if(los.nextDouble()<0.5){
					ustawSkret(prawoSkret(dajZwrot()));
				}
				else{
					ustawSkret(lewoSkret(dajZwrot()));
				}
			}
		}
		else{
			if(los.nextDouble()<0.5){
				ustawSkret(prawoSkret(dajSkret()));
			}
			else{
				ustawSkret(lewoSkret(dajSkret()));
			}
		}
	}
}
