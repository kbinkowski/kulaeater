package kulaeater.model;

import static kulaeater.stale.Stale.*;
import kulaeater.stale.Kierunek;
import static kulaeater.stale.RodzajPola.*;
import kulaeater.stale.RodzajPola;



/**Klasa reprezentuje plansz� gry. Zawiera m.in. informacje o ka�dym jej polu, pocz�tkowe pozycje postaci, czas
 * przewidziany na jej uko�czenie
 * @author damgad
 *
 */
public class Plansza implements Cloneable {
	
	/**Numer planszy.
	 * 
	 */
	private int numer;
	/**Czas przewidziany na uko�czenie planszy.
	 * 
	 */
	private int czas;
	/**Pocz�tkowa pozycja KulaEatera - pierwsze pole tablicy - X, drugie - Y
	 * 
	 */
	private int[] pozycjaKe;
	/**Pocz�tkowa pozycja Wrogow- pierwsze pole tablicy - X, drugie - Y
	 * 
	 */
	private int[] pozycjaWrog;
	/**Ilo�� Wrog�w przewidzanych na plansz�.
	 * 
	 */
	private int iloscWrog;
	/**Ilo�� kulek koniecznych do zjedzenia aby przejsc plansz�.
	 * 
	 */
	private int iloscKulek;
	/**Dwuwymiarowa tablica reprezentuj�ca pola planszy.
	 * 
	 */
	private RodzajPola[][] tabPlanszy;
	
	/**Kontruktor planszy ustawia odpowiednie pola.
	 * @param numer
	 * @param tabPlanszy
	 * @param czas
	 * @param pozycjaKe
	 * @param pozycjaWrog
	 * @param iloscWrog
	 * @param iloscKulek
	 */
	Plansza(int numer, RodzajPola[][] tabPlanszy, int czas, int[] pozycjaKe, int[] pozycjaWrog, int iloscWrog, int iloscKulek){
		this.numer = numer;
		this.czas = czas;
		this.pozycjaKe = pozycjaKe;
		this.pozycjaWrog = pozycjaWrog;
		this.iloscWrog = iloscWrog;
		this.iloscKulek = iloscKulek;
		this.tabPlanszy = tabPlanszy;
	}
	public Plansza clone(){
		RodzajPola[][] nowaTab = new RodzajPola[KAFLE_NA_WYS][KAFLE_NA_SZER];
		for(int i=0; i<KAFLE_NA_WYS;i++){
			for(int j=0; j<KAFLE_NA_SZER;j++){
				nowaTab[i][j]=tabPlanszy[i][j];
			}
		}
		int[] nowaPozKe = {pozycjaKe[0], pozycjaKe[1]};
		int[] nowaPozWr = {pozycjaWrog[0], pozycjaWrog[1]};
		return new Plansza(numer,nowaTab,czas,nowaPozKe,nowaPozWr,iloscWrog,iloscKulek);
	}
	/**
	 * @return numer planszy
	 */
	public int dajNumer(){
		return numer;
	}
	/**
	 * @return czas przewidziany na ukonczenie planszy
	 */
	public int dajCzas(){
		return czas;
	}
	/**
	 * @return pocz�tkowa pozycja kulaeatera - pierwsze pole tablicy - X, drugie - Y
	 */
	public int[] dajPozycjaKe(){
		return pozycjaKe;
	}

	/**
	 * @return pocz�tkowa pozycja wrogow - pierwsze pole tablicy - X, drugie - Y
	 */
	public int[] dajPozycjaWrog(){
		return pozycjaWrog;
	}
	/**
	 * @return ilosc wrogow
	 */
	public int dajIloscWrog(){
		return iloscWrog;
	}
	/**
	 * @return ilo�� kulek konieczna do zjedzenia
	 */
	public int dajIloscKulek(){
		return iloscKulek;
	}
	/**
	 * @return Dwuwymiarowa tablica reprezentuj�ca pola planszy.
	 */
	public RodzajPola[][] dajUklad(){
		return tabPlanszy;
	}
	
	/**Metoda bada czy pomi�dzy dwoma polami planszy istnieje bezpo�rednia widoczno�� w linii prostej
	 * (poziomo lub pionowo). 
	 * @param xa pozycja x pierwszego pola
	 * @param ya pozycja y pierwszego pola
	 * @param xb pozycja x drugiego pola
	 * @param yb pozycja y drugiego pola
	 * @return true kiedy na drodze niema muru, inaczej false
	 */
	boolean czyMurNaDrodze(int xa, int ya, int xb, int yb){
		boolean wid=true;
		if(xa==xb){
			while(ya!=yb){
				if (jakiePole(ya,xa)==MUR) {
					
					wid=false;
				}
				ya+=ya<yb?1:-1;
			}
		}
		else if (ya==yb){
			while(xa!=xb){
				if (jakiePole(ya,xa)==MUR){
					wid=false;
					
				}
				xa+=xa<xb?1:-1;
			}
		}
		else wid=false;
		return wid;
	}
	
	/**Metoda bada w jakiej odleg�o�ci od danej pozycji znajduje si� (w danym kierunku mur)
	 * @param x pozycja x (pozioma)
	 * @param y pozycja y (pionowa)
	 * @param kierunek w jakim kierunku chcemy mierzyc
	 * @return odleg�o�� od muru wyra�ona w polach (l. rzeczywista)
	 */
	double ileDoMuru(double x, double y, Kierunek kierunek){
		int i;
		double ile=0;
		double xCalk;
		double xUlamk;	
		double yCalk;
		double yUlamk;
		
		switch(kierunek){
		case LEWO :
			xCalk=Math.floor(x);
			xUlamk=x-xCalk;	
			yCalk=Math.floor(y);
			yUlamk=y-yCalk;
			for (i=0; jakiePole((int)yCalk,(int)xCalk-i-1)!=MUR; ++i){}
			ile=xUlamk+i;
			break;
		case PRAWO :
			xCalk=Math.ceil(x)-1;
			xUlamk=x-xCalk;	
			yCalk=Math.floor(y);
			yUlamk=y-yCalk;
			for (i=0; jakiePole((int)yCalk,(int)xCalk+i+2)!=MUR; ++i){}
			ile=1-xUlamk+i;
			break;
		case GORA :
			xCalk=Math.floor(x);
			xUlamk=x-xCalk;	
			yCalk=Math.floor(y);
			yUlamk=y-yCalk;
			for (i=0; jakiePole((int)yCalk-i-1,(int)xCalk)!=MUR; ++i){}
			ile=yUlamk+i;
			break;
		case DOL :
			xCalk=Math.floor(x);
			xUlamk=x-xCalk;	
			yCalk=Math.ceil(y)-1;
			yUlamk=y-yCalk;
			for (i=0; jakiePole((int)yCalk+i+2,(int)xCalk)!=MUR; ++i){}
			ile=1-yUlamk+i;
			break;
		default:
			break;
		}
		return ile;
	}
		
	 
	/**Metoda wype�nia ka�de puste pole planszy zwyk�� kulk�.
	 * 
	 */
	void wypelnijKulkami(){
		for(int i=0; i<KAFLE_NA_WYS;i++){
			for(int j=0; j<KAFLE_NA_SZER;j++){
				if (tabPlanszy[i][j].equals(PUSTKA)) 
					tabPlanszy[i][j]=KULKOWE;
			}
		}
	}
	
	/**bada rodzaj pola
	 * @param y pozycja y 
	 * @param x pozycja x
	 * @return rodzaj pola na danej pozycji
	 */
	public RodzajPola jakiePole(int y, int x){
		return tabPlanszy[y][x];
	}
	public void usunKulke(int x, int y) {
		tabPlanszy[y][x]=PUSTKA;
	}
	
}
