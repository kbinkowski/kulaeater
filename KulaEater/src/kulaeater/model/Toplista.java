package kulaeater.model;

import java.util.Map;

/** Klasa reprezentuje list� najlepszych graczy.
 * @author KE team
 *
 */
public class Toplista{ 
	
	/**Mapa gdzie identyfikatorem jest pozycja na li�cie, a warto�ci� napis - imi�  i ilo�� punkt�w
	 * 
	 */
	private Map<Integer, String> topMapa;
	
	/**Konstruktor tworz�cy toplist� z mapy.
	 * @param topMapa
	 */
	Toplista(Map<Integer,String> topMapa){
    	this.topMapa=topMapa;

    }

	/**
	 * @return mapa najelpszych u�ytkowk�w gdzie identyfikatorem jest pozycja na li�cie, a warto�ci� napis - imi�  i ilo�� punkt�w
	 */
	public Map<Integer,String> dajTopMapa() {
		return topMapa;
	}
	
	/** Ustawia map� u�ytkownik�w.
	 * @param topMapa identyfikatorem jest pozycja na li�cie, a warto�ci� napis - imi�  i ilo�� punkt�w
	 */
	public void ustawTopMapa(Map<Integer, String> topMapa){
		this.topMapa=topMapa;
	}
}