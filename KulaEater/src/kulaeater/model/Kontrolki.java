package kulaeater.model;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**Klasa reprezentuje kontrolki przechowujące wskaźniki stanu gry.
 * @author K-E team
 *
 */
public class Kontrolki  implements ActionListener {
	
	/**Pozostałe życia Pacmana.
	 * 
	 */
	private int pozostaleZycia;
	/**Nazwa aktywnego bonusu.
	 * 
	 */
	private String nazwaBonusu;
	/**Napis - nazwa aktywnego poziomu.
	 * 
	 */
	private String nazwaPoziomu;
	/**Liczba zdobytych punktów
	 * 
	 */
	private int zdobytePunkty;
	/**Czas pozostały do ukończenia poziomu.
	 * 
	 */
	private int czas;
	/**Timer zmniejszający licznik czasu - 'czas'.
	 * 
	 */
	private final Timer t;
		
	/**Konstruktor kontrolek ustawia odpowiednie pola.
	 * @param zycia
	 * @param bonus
	 * @param poziom
	 * @param punkty
	 * @param czas
	 */
	Kontrolki(int zycia, String bonus, String poziom, int punkty, int czas){
		this.pozostaleZycia = zycia;
		this.nazwaBonusu = bonus;
		this.nazwaPoziomu = poziom;
		this.zdobytePunkty = punkty;
		this.czas = czas;
		
		t = new Timer(1000, this);
		t.start();
	}
	
	
	
	public int dajZycia() {
		return pozostaleZycia;
	}
	public void ustawZycia(int zycia) {
		this.pozostaleZycia = zycia;
	}
	public String dajBonus() {
		return nazwaBonusu;
	}
	public void ustawBonus(String bonus) {
		this.nazwaBonusu = bonus;
	}
	public String dajPoziom() {
		return nazwaPoziomu;
	}
	public void ustawPoziom(String poziom) {
		this.nazwaPoziomu = poziom;
	}
	public int dajPunkty() {
		return zdobytePunkty;
	}
	public void ustawPunkty(int punkty) {
		this.zdobytePunkty = punkty;
	}
	public int dajCzas() {
		return czas;
	}
	public void ustawCzas(int czas) {
		this.czas = czas;
	}
	@Override
	public void actionPerformed(ActionEvent ae) {
		--czas;
		
	}
}
