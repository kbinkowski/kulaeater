package kulaeater.model;

import static kulaeater.stale.RodzajPola.*;
import static kulaeater.stale.Kierunek.*;
import kulaeater.stale.*;

/**Klasa reprezentuje istot� - posta� w grze. Pozwala no okreslenie jej pozycji, pr�dko�ci..
 * @author K-E team
 *
 */
abstract class Istota {
	
	/**Pozycja istotny wyra�ona w kaflach (polach).
	 * 
	 */
	private double x,y;
	
	/**Zwrot istoty - przechowuje kierunek w ktorym obecnie si� ona porusza.
	 * 
	 */
	private Kierunek zwrot = ZERO;
	
	/**Przechowuje kierunek w ktorym istota "chce" pod��a� gdy tylko obecna pozycja jej na to pozwoli (brak muru).
	 * 
	 */
	private Kierunek skret = ZERO;
	
	/**Przechowuje referencj� na model do kt�rego nale�y istota.
	 * 
	 */
	protected final Model mojModel;
	
	/**Przechowuje predkosc istoty w kaflach (polach) na milisekund�.
	 * 
	 */
	protected double predkosc;
	
	///Konstruktor Istoty
	/**Konstruktor Istoty
	 * @param model model do ktorego ma nale�ej
	 * @param pozX pozycja poczatkowa X
	 * @param pozY pozycja poczatkowa Y
	 * @param predkosc pr�dko�� pocz�tkowa
	 */
	Istota(Model model, double pozX, double pozY, double predkosc){
		mojModel=model;
		x=pozX;
		y=pozY;
		this.predkosc=predkosc;
	}
		
	
	
	/**W zaleznosci od aktualnego zwrotu postaci i jej predkosci, zmienia jej po�o�enie dt*predkosc.  
	 * Metoda ta sprawdza czy na drodze (w aktualnej planszy) przesuni�cia wyst�puj� przeszkody. 
	 * Je�li tak, dosuwa do przeszkody.
	 * @param dt parametr okre�laj�cy czas po jakim nast�puje przemieszczenie
	 */
	private void przesun(float dt){
		if(czyMogeWBok()) zwrot=skret;
		double przemieszczenie=ileMogePrzedSiebie(predkosc*dt);
		//if(czyMogePrzedSiebie(przemieszczenie)){
			switch(zwrot){
				case LEWO :
					ustawPozycje(dajPozycjeX()-przemieszczenie,Math.round(dajPozycjeY()));
					break;
				case PRAWO :
					ustawPozycje(dajPozycjeX()+przemieszczenie,Math.round(dajPozycjeY()));
					break;
				case GORA :
					ustawPozycje(Math.round(dajPozycjeX()),dajPozycjeY()-przemieszczenie);
					break;
				case DOL :
					ustawPozycje(Math.round(dajPozycjeX()),dajPozycjeY()+przemieszczenie);
					break;
			//}
		}
	}
	/**Aktualizuje pozycj� istoty.
	 * @param czasPoprzedni poprzedni czas aktualizacji, slozy do obliczenia odstepu czasu miedzy aktualizacjami.
	 */
	void aktualizuj(long czasPoprzedni){
		long dt = System.currentTimeMillis() - czasPoprzedni;
		przesun(dt);
	}
	/*
	public void aktualizuj(long czasPoprzedni){
		long dt = System.currentTimeMillis() - czasPoprzedni;
		if(czyMogeWBok(skret)){
			switch (skret){
				case LEWO :
					dx=-predkosc;
					dy=0;
					break;
				case PRAWO :
					dx=predkosc;
					dy=0;
					break;
				case GORA :
					dx=0;
					dy=-predkosc;
					break;
				case DOL :
					dx=0;
					dy=predkosc;
					break;
				default:
					dx=0;
					dy=0;
			}
		}
		przesun(dt);
			
	}*/
	/**Sprawdza o ile mo�na przesun�� posta� aby nie przej�� przez mur.
	 * @param przemieszczenie o ile chcemy przesun��
	 * @return o ile mo�na przesuna� (nie wi�cej niz @param przemieszenie)
	 */
	protected double ileMogePrzedSiebie(double przemieszczenie){
		return Math.min(mojModel.obecnaPlansza.ileDoMuru(dajPozycjeX(),dajPozycjeY(),dajZwrot()),przemieszczenie);
	}
	
	/**Sprawdza czy istota mo�e skr�ci� w zwi�zu z ch�ci� wyra�on� polem 'skret'.
	 * @return true=mo�e
	 */
	protected boolean czyMogeWBok(){
		//return mojModel.obecnaPlansza.ileDoMuru((int)Math.floor(x),(int)Math.floor(y)-1, skret)<przemieszczenie;
		boolean czy=false;
		
		if((zwrot==LEWO | zwrot==PRAWO) && skret==GORA) {
			czy = mojModel.obecnaPlansza.jakiePole((int)Math.floor(y)-1,(int)Math.round(x))==MUR ? false : true;
		}
		else if((zwrot==LEWO | zwrot==PRAWO) && skret==DOL) {
			czy = mojModel.obecnaPlansza.jakiePole((int)Math.floor(y)+1,(int)Math.round(x))==MUR ? false : true;
		}
		else if((zwrot==GORA | zwrot==DOL) && skret==PRAWO) {
			czy = mojModel.obecnaPlansza.jakiePole((int)Math.round(y),(int)Math.floor(x)+1)==MUR ? false : true;
		}
		else if((zwrot==GORA | zwrot==DOL) && skret==LEWO) {
			czy = mojModel.obecnaPlansza.jakiePole((int)Math.round(y),(int)Math.floor(x)-1)==MUR ? false : true;
		}
		else{
			czy=true;
		}
		return czy;
	
}



	/**
	 * @return W kaflach (polach)
	 */
	public double dajPozycjeX(){
		return x;
	}
	/**
	 * @return W kaflach (polach)
	 */
	public double dajPozycjeY(){
		return y;
	}

	/**
	 * @return aktualny kierunek przemieszczania si�
	 */
	public Kierunek dajZwrot(){
		return zwrot;
	}
	
	/**
	 * @return aktualny kierunek w kt�rym istota 'chce' skr�ci�
	 */
	public Kierunek dajSkret(){
		return skret;
	}
	/**Ustawia kierunek przemieszczania si� istoty
	 * @param zwrot 
	 */
	protected synchronized void ustawZwrot(Kierunek zwrot){
		this.zwrot=zwrot;
	}
	/**
	 * @param x w polach (kaflach)
	 * @param y w polach (kaflach)
	 */
	protected synchronized void ustawPozycje(double x, double y){
		this.x=x;
		this.y=y;
	}
	protected synchronized void ustawPredkosc(double predkosc){
		this.predkosc=predkosc;
	}
	/**Ustawia ch�� skr�tu w danym kierunku
	 * @param skret
	 */
	protected synchronized void ustawSkret(Kierunek skret){
		this.skret=skret;
	}
}
