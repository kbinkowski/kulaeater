package kulaeater;
import kulaeater.sterownik.Sterownik;

/** Klasa zawieraj�ca g��wn� funkcj� (main) gry. 
 * @author KE team
 *
 */
public class KulaEater {
	/**Tworzy Sterownik i uruchamia go pozwalaj�c na zaj�cie w�tku maina przez niego.
	 * @param args
	 */
	public static void main(String[] args){
		Sterownik s = new Sterownik();
		s.dzialaj();
	}
}
