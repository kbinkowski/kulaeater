package kulaeater.stale;

/**Typ wyliczeniowy reprezentujący typ pola.
 * @author K-E team
 *
 */
public enum RodzajPola {
	MUR, PUSTKA, KULKOWE,
	B_PRZYSPIESZ, B_JEDZWROGOW, B_WOLNYWROG, B_ZABIJWROGOW, B_ZYCIE, B_WOLNYPACMAN, B_ZAMROZ, B_SZYBKIWROG, B_SMIERC, B_ZASLONA
}
