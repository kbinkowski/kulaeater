package kulaeater.stale;

/**Interfejs zawieraj�cy wszelkie sta�e ora� �cie�ki wykorzystywane w grze.
 * @author K-E team
 *
 */
public interface Stale {
	int KAFLE_NA_SZER = 55;
	int KAFLE_NA_WYS = 33;
	int WYMIAR_KAFLA = 15;
	int WYSOKOSC_POLA_GRY = KAFLE_NA_WYS*WYMIAR_KAFLA;
	int SZEROKOSC_POLA_GRY = KAFLE_NA_SZER*WYMIAR_KAFLA;
	int WYSOKOSC_PANELU_KONTROLNEGO = 50;
	int CZAS_BONUSU_SEK = 5;

	String SCIEZKA_TOPLISTA_XML = "./dane/top.xml";
	String SCIEZKA_PARAMETRY_XML = "./dane/parametry.xml";
	//menu
	String SCIEZKA_MENU_TLO = "images/menu/tlo.gif"; 
	String SCIEZKA_MENU_AUTORZY = "images/menu/autorzy.gif"; 
	String SCIEZKA_MENU_GRAJ = "images/menu/graj.gif"; 
	String SCIEZKA_MENU_TOPLISTA = "images/menu/top10.gif"; 
	String SCIEZKA_MENU_KONIEC= "images/menu/koniec.gif";
	String SCIEZKA_MENU_OFFLINE= "images/menu/offline.gif"; 
	String SCIEZKA_MENU_ONLINE= "images/menu/online.gif"; 
	// panel kontrolny
	String SCIEZKA_PANEL_KONTROLNY_ZYCIE ="images/mapa/zycie.png"; 
	//okno planszy
	String SCIEZKA_OKNO_PLANSZY_MUR="images/mapa/mur.png";
	String SCIEZKA_OKNO_PLANSZY_KULKA="images/mapa/kulka.png";
	String SCIEZKA_OKNO_PLANSZY_BONUSOWA_KULKA="images/mapa/bonusowakulka.png";
	String SCIEZKA_OKNO_PLANSZY_DROGA="images/mapa/droga.png";
	String PACMAN_PRAWO="images/mapa/pacman_prawo.png";
	String PACMAN_LEWO="images/mapa/pacman_lewo.png";
	String PACMAN_GORA="images/mapa/pacman_gora.png";
	String PACMAN_DOL="images/mapa/pacman_dol.png";
	String SCIEZKA_OKNO_PLANSZY_ZASLONA="images/mapa/zaslona.png";
	String SCIEZKA_OKNO_PLANSZY_WROG="images/mapa/duch.png";


	String BONUS_NAZWA_ZJADANIEWROGOW="Mo�esz zjada� duchy! :)";
	String BONUS_NAZWA_ZYCIE="Otrzyma�e� dodatkowe �ycie :)";
	String BONUS_NAZWA_ZABIJWSZYSTKICH="Zabiles wszystkich wrog�w :)";
	String BONUS_NAZWA_PRZYSPIESZENIE="Przyszpieszenie :)";
	String BONUS_NAZWA_WOLNYWROG="Wrogowie zwalniaj� :)";
	String BONUS_NAZWA_SZYBKIWROG="Wrogowie przyspieszaja :(";
	String BONUS_NAZWA_ZWOLNIENIE="Spowolnienie :(";
	String BONUS_NAZWA_ZAMROZENIE="Zostales zamrozony :(";
	String BONUS_NAZWA_SMIERC="Tracisz zycie :(";
	String BONUS_NAZWA_ZASLONA="Ograniczenie widocznosci :(";
	
	String BLAD_PACZENIA_SPROBUJ_PONOWNIE="B��d po��czenia. Spr�buj ponownie.";
	
}
