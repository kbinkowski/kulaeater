package kulaeater.stale;

import kulaeater.stale.Kierunek;


/**Typ Wyliczeniowy reprezentuj�cy kierunek ruchu KulaEatera i wrog�w.
 * @author K-E team
 *
 */
public enum Kierunek {
	PRAWO, LEWO, GORA, DOL, ZERO;
	
	/**Metoda zwracaj�ca przeciwny zwrot do danego.
	 * @param obecny Obecny zwrot.
	 * @return Zwrot przeciwny do obecnego.
	 */
	public static Kierunek przeciwny(Kierunek obecny){
		switch (obecny){
		case PRAWO:
			return LEWO;
		case LEWO:
			return PRAWO;
		case GORA:
			return DOL;
		case DOL:
			return GORA;
		default :
			return ZERO;
		}
	}
	
	/**Metoda zwracaj�ca kierunek po skr�cie w prawo z danego kierunku.
	 * @param obecny Obecny kierunek.
	 * @return Kierunek po skr�cie.
	 */
	public static Kierunek prawoSkret(Kierunek obecny){
		switch (obecny){
		case PRAWO:
			return DOL;
		case LEWO:
			return GORA;
		case GORA:
			return PRAWO;
		case DOL:
			return LEWO;
		default :
			return PRAWO;
		}
	}
	/**Metoda zwracaj�ca kierunek po skr�cie w lewo z danego kierunku.
	 * @param obecny Obecny kierunek.
	 * @return Kierunek po skr�cie.
	 */
	public static Kierunek lewoSkret(Kierunek obecny){
		switch (obecny){
		case PRAWO:
			return GORA;
		case LEWO:
			return DOL;
		case GORA:
			return LEWO;
		case DOL:
			return PRAWO;
		default :
			return LEWO;
		}
	}
}