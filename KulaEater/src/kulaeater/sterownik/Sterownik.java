package kulaeater.sterownik;
import java.util.concurrent.LinkedBlockingQueue;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

import kulaeater.widok.*;
import kulaeater.model.*;
import kulaeater.model.zdarzenia.*;
import kulaeater.sterownik.strategie.*;
import kulaeater.widok.zdarzenia.*;

/**Klasa odpowiedzialna za wymian� informacji pomi�dzy Widokiem a Modelem.
 * Zawiera kolejk� zdarze� z kt�rej odczytuje je i wykonuje przypisane do nich strategie.
 * @author K-E team
 *
 */
public class Sterownik{
	/**Kolejka zdarze� generowanych w grze.
	 * 
	 */
	private final BlockingQueue<Event> kolejka;
	/**Referencja na Model odpoweidzialny za logik� gry.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok odpoweidzialny za prezentacje graficzn� gry.
	 * 
	 */
	private final Widok widok;
	/**Mapa wi���ca zdarzenia z odpowiadaj�cymi im strategiami.
	 * 
	 */
	private Map<Class<? extends Event>,Strategia> mapa;
	
	/**Konstruktor klasy Sterownik. Tworzy kolejk� zdarze�, ustawia model i widok. Dodaje r�wnie� do mapy
	 * klasy zdarze� i odpowiadaj�ce im strategie dzia�ania.
	 * 
	 */
	public Sterownik(){
		kolejka = new LinkedBlockingQueue<Event>();
		mapa = new HashMap<Class<? extends Event>, Strategia>();
		widok = new Widok(kolejka);
		model = new Model(kolejka);
		mapa.put(PokazToplisteEvent.class, new PokazToplisteStrategia(model,widok));
		mapa.put(PokazAutorowEvent.class, new PokazAutorowStrategia(model,widok));
		mapa.put(GrajEvent.class, new GrajStrategia(model,widok));
		mapa.put(ZakonczEvent.class, new ZakonczStrategia(model,widok));
		mapa.put(DoMenuEvent.class, new DoMenuStrategia(model,widok));
		mapa.put(ZmianaSkretuEvent.class, new ZmianaSkretuStrategia(model,widok));
		mapa.put(WidokOnlineEvent.class, new OnlineStrategia(model,widok));
		mapa.put(ModelOnlineEvent.class, new OnlineStrategia(model,widok));
		mapa.put(NowyStanArenyEvent.class, new NowyStanArenyStrategia(model,widok));
		mapa.put(BladEvent.class, new BladStrategia(model,widok));
		mapa.put(KoniecSerwerEvent.class, new KoniecSerwerStrategia(model,widok));
		mapa.put(PauzaEvent.class,new PauzaStrategia(model,widok));
		mapa.put(CheatKolejnyPoziomEvent.class, new CheatKolejnyPoziomStrategia(model,widok));
		mapa.put(DzwiekEvent.class, new DzwiekStrategia(model,widok));
	}
	
	/**Metoda uruchamiaj�ca sterownik. Tworzy w�tek modelu i uruchamia w nim model.
	 * Nast�pnie zajmuje w�tek w kt�rym zosta�a wywo�ana na reagowanie na zdarzenia.
	 * 
	 */
	public void dzialaj(){
		Event zdarzenie = null;
		Thread watekModelu = new Thread(model);
		watekModelu.start();
		while (true){
			try{
				zdarzenie = kolejka.take();
			}
			catch (InterruptedException e){
				kolejka.add(new BladEvent("Bl�d obs�ugi zdarze�."));	
			}
			mapa.get(zdarzenie.getClass()).wykonaj(zdarzenie);
		}
	}
}
