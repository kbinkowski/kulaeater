package kulaeater.sterownik.strategie;


import kulaeater.model.CzytnikPlikow;
import kulaeater.model.KlientSieciowy;
import kulaeater.model.Model;
import kulaeater.model.zdarzenia.ModelOnlineEvent;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.WidokOnlineEvent;
import kulaeater.widok.zdarzenia.Event;
import static kulaeater.stale.Stale.*;

/**Strategia przełączająca tryb gry.
 * @author K-E team
 *
 */
public class OnlineStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy OnlineStrategia.
	 * @param model
	 * @param widok
	 */
	public OnlineStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	
	@Override
	public void wykonaj(Event e) {
		
		
		if (e instanceof WidokOnlineEvent ){
			WidokOnlineEvent zdarzenie = (WidokOnlineEvent) e;
			if(zdarzenie.dajTryb()){
				String adres=widok.zapytajAdresSerwera();
				if (adres!=null){
					try{
						model.ustawZrodloDanych(new KlientSieciowy(model, adres.split(":")[0], adres.split(":")[1]));
						widok.wyswietlTrybOnline(true);
					} catch(Exception w){
						widok.wyswietlKomunikat(BLAD_PACZENIA_SPROBUJ_PONOWNIE+w.toString());
					}											
				}
			}
			else if(!zdarzenie.dajTryb()){
				model.ustawZrodloDanych(new CzytnikPlikow(model));
				widok.wyswietlTrybOnline(false);
			}
		}
		else if (e instanceof ModelOnlineEvent ){
			ModelOnlineEvent event = (ModelOnlineEvent) e;
			if(event.dajTryb()){
				String adres=widok.zapytajAdresSerwera();
				if(adres!=null){
					try{
						model.ustawZrodloDanych(new KlientSieciowy(model, adres.split(":")[0], adres.split(":")[1]));
						widok.wyswietlTrybOnline(true);
					} catch(Exception w){
						widok.wyswietlKomunikat(BLAD_PACZENIA_SPROBUJ_PONOWNIE+w.toString());
					}
				}
			}
			else if(!event.dajTryb()){
				model.ustawZrodloDanych(new CzytnikPlikow(model));
				widok.wyswietlTrybOnline(false);
			}
		}
	}

}
