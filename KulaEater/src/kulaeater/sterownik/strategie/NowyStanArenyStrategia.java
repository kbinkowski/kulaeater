package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.model.zdarzenia.NowyStanArenyEvent;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia przełączająca stan gry.
 * @author K-E team
 *
 */
public class NowyStanArenyStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;

	/**Konstruktor klasy NowyStanArenyStrategia
	 * @param model
	 * @param widok
	 */
	public NowyStanArenyStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	@Override
	public void wykonaj(Event zdarzenie) {
		NowyStanArenyEvent n = (NowyStanArenyEvent)zdarzenie;
		
		widok.aktualizujKontrolki(n.dajKontrolki().dajZycia(), n.dajKontrolki().dajPoziom(), n.dajKontrolki().dajBonus(), n.dajKontrolki().dajPunkty(), n.dajKontrolki().dajCzas());
		widok.aktualizujPlansze(n.dajUkladPlanszy(), n.dajPozPacman(), n.dajZwrotPacman(), n.dajPozWrog(), n.dajZaslona());
	}

}
