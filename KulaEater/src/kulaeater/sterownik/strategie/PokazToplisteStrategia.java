package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia przełaczająca widok na Toplistę.
 * @author K-E team
 *
 */
public class PokazToplisteStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;

	/**Konstruktor klasy PokazToplisteStrategia.
	 * @param model
	 * @param widok
	 */
	public PokazToplisteStrategia(final Model model, final Widok widok){
		this.model = model;
		this.widok = widok;
	}
	@Override
	public void wykonaj(Event zdarzenie){
		widok.wyswietlTopliste(model.dajToplista().dajTopMapa());
	}
}
