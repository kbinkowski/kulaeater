package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia reaguj�ca na zerwanie po��czenia z serwerem.
 * @author K-E team
 *
 */
public class KoniecSerwerStrategia extends Strategia {
	/**Referencja na Model obs�ugiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obs�ugiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy KoniecSerwerStrategia.
	 * @param model
	 * @param widok
	 */
	public KoniecSerwerStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	
	@Override
	public void wykonaj(Event zdarzenie) {
		if(model.czyTrwaRozgrywka()){
			widok.wyswietlKomunikat("Serwer ko�czy dzia�anie. Nast�pi przerwanie gry oraz prze��czenie na tryb offline.");
			model.zakonczRozgrywke();
			widok.wrocDoMenu();
		}
		else
			widok.wyswietlKomunikat("Serwer ko�czy dzia�anie. Nast�pi prze��czenie na tryb offline.");
	}

}
