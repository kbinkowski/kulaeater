package kulaeater.sterownik.strategie;


import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.model.zdarzenia.BladEvent;
import kulaeater.widok.zdarzenia.Event;

/**Strategia obs�uguj�ca zdarzenie b��du. Wy�wietla na ekranie komunikat o b��dzie. 
 * @author K-E team
 *
 */
public class BladStrategia extends Strategia {
	/**Referencja na Model obs�ugiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obs�ugiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy BladStrategia. 
	 * @param model
	 * @param widok
	 */
	public BladStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	
	@Override
	public void wykonaj(Event zdarzenie) {
		BladEvent e =(BladEvent) zdarzenie;
		widok.wyswietlKomunikat(e.dajTekst());
	}

}
