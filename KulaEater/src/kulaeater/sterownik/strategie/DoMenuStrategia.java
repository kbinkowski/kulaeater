package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia przełączająca widok do menu.
 * @author K-E team
 *
 */
public class DoMenuStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy DoMenuStrategia.
	 * @param model
	 * @param widok
	 */
	public DoMenuStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	
	@Override
	public void wykonaj(Event zdarzenie) {
		if(model.czyTrwaRozgrywka()){
			int wynik=model.zakonczRozgrywke();
			boolean decyzja=model.czyWynikDobryNaTopliste(wynik);
			if(decyzja){
				String imie = widok.zapytajImieGracza();
				model.dodajWpisToplisty(wynik, imie);
			}
		}
		widok.wrocDoMenu();
		
	}

}
