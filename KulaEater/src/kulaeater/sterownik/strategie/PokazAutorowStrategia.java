package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia przełączająca widok na prezentacje informacji o autorach gry.
 * @author K-E team
 *
 */
public class PokazAutorowStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy PokazAutorowStrategia.
	 * @param model
	 * @param widok
	 */
	public PokazAutorowStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	
	@Override
	public void wykonaj(Event zdarzenie) {
		widok.wyswietlAutorow(model.dajAutorow());
	}

}
