package kulaeater.sterownik.strategie;
import kulaeater.widok.zdarzenia.Event;
/**Klasa abstrakcyjna reprezentująca strategie na przyjście zdarzenia.
 * @author K-E team
 *
 */
public abstract class Strategia {
	/**Metoda abstrakcyjna wykonująca operacje pod wpływem nadejścia zdarzenia.
	 * @param zdarzenie 
	 */
	public abstract void wykonaj(Event zdarzenie);
}
