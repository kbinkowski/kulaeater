package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

public class ZjedzonaKulaStrategia extends Strategia{
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy ZjedzonaKulaStrategia.
	 * @param model
	 * @param widok
	 */
	public ZjedzonaKulaStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	@Override
	public void wykonaj(Event zdarzenie) {
		widok.startDzwiekKulka();
		
	}

}
