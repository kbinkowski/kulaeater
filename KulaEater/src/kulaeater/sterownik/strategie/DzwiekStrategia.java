package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.model.zdarzenia.DzwiekEvent;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

public class DzwiekStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy ZabityKulaEaterKulaStrategia.
	 * @param model
	 * @param widok
	 */
	public DzwiekStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	@Override
	public void wykonaj(Event zdarzenie) {
		DzwiekEvent event = (DzwiekEvent) zdarzenie;
		String polecenie = event.dajRodzaj();
		switch (polecenie){
		case "smierc":
			widok.startDzwiekSmierc();
			break;
		case "zjedzenie":
			widok.startDzwiekKulka();
			break;
		default:
			break;
		}
		
		
		
	}

}
