package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia przenosz�ca gr� do nast�pnego poziomu. G��wnie w ramach test�w.
 * @author K-E team
 *
 */
public class CheatKolejnyPoziomStrategia extends Strategia {
	/**Referencja na Model obs�ugiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obs�ugiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy CheatKolejnyStrategia.
	 * @param model
	 * @param widok
	 */
	public CheatKolejnyPoziomStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	@Override
	public void wykonaj(Event zdarzenie) {
		model.cheatKolejnyPoziom();
	}

}
