package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia uruchamiająca rozgrywkę. 
 * @author K-E team
 *
 */
public class GrajStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy GrajStrategia.
	 * @param model
	 * @param widok
	 */
	public GrajStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	
	@Override
	public void wykonaj(Event zdarzenie) {
		
		widok.wyswietlRozgrywke();
		model.startujRozgrywke();
		
	}
	

}
