package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;
import kulaeater.widok.zdarzenia.ZmianaSkretuEvent;

/**Strategia zmieniająca skręt istoty.
 * @author K-E team
 *
 */
public class ZmianaSkretuStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;

	/**Konstruktor klasy ZmianaSkretuStrategia.
	 * @param model
	 * @param widok
	 */
	public ZmianaSkretuStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	
	@Override
	public void wykonaj(Event zdarzenie) {
		ZmianaSkretuEvent zmiana = (ZmianaSkretuEvent)zdarzenie;
		model.skrecPacmana(zmiana.dajKierunek());

	}

}
