package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia przełączająca Pauze w grze.
 * @author K-E team
 *
 */
public class PauzaStrategia extends Strategia {
	/**Referencja na Model obsługiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obsługiwany.
	 * 
	 */
	private final Widok widok;
	
	/**Konstruktor klasy PauzaStrategia.
	 * @param model
	 * @param widok
	 */
	public PauzaStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	@Override
	public void wykonaj(Event zdarzenie) {
		if(model.czyTrwaRozgrywka()) model.wlaczPauze();
	}

}
