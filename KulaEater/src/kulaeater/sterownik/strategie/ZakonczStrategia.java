package kulaeater.sterownik.strategie;

import kulaeater.model.Model;
import kulaeater.widok.Widok;
import kulaeater.widok.zdarzenia.Event;

/**Strategia wy��czaj�ca gr�.
 * @author K-E team
 *
 */
public class ZakonczStrategia extends Strategia {
	/**Referencja na Model obs�ugiwany.
	 * 
	 */
	private final Model model;
	/**Referencja na Widok obs�ugiwany.
	 * 
	 */
	private final Widok widok;

	/**Konstruktor klasy ZakonczStrategia.
	 * @param model
	 * @param widok
	 */
	public ZakonczStrategia(final Model model, final Widok widok) {
		this.model = model;
		this.widok = widok;
	}
	
	@Override
	public void wykonaj(Event zdarzenie) {
		model.zakoncz();
		System.exit(0);
	}

}
